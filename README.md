# Tiny Opcode Interpreter

![tiny opcode interpreter icon](src/images/pixels/logo-large.png)

# About

This is a specification, assembler and interpeter for an instruction set.

# Dependencies

This is a client-side web project, using typescript, npm and parcel to bundle resources.

# Debugging

There's a VSCode launch configuration that can be used to debug in chrome.

# Build commands

`npm run ...`

| command      | description                            |
| ------------ | -------------------------------------- |
| `dev`        | starts a dev loop on localhost         |
| `serve`      | builds and serves on localhost         |
| `build`      | builds without source maps             |
| `lint`       | runs source code checks and formatting |
| `test`       | runs tests                             |
| `test:watch` | runs tests in watch mode               |

# Specification

## Writing down numbers

This document uses different notation for numeric values: _decimal_, _hexadecimal_ and _binary_.

The digits `10` mean a different value in each of the notations.

### Decimal

Decimal notation uses ten different digits, `0` to `9`, so `0 1 2 3 4 5 6 7 8 9` means _zero_ to _nine_.

So `10` in decimal means _ten_.

| _1000's_ | _100's_ | _10's_ | _1's_ |
| -------: | ------: | -----: | ----: |
|        0 |       0 |      1 |     0 |

### Hexadecimal

Hexadecimal notation uses sixteen different digits, `0` to `9` but also the letters `a` to `f`, so `0 1 2 3 4 5 6 7 8 9 a b c d e f` meands _zero_ to _fifteen_.

So `10` in hexadecimal means _sixteen_.

| _4096's_ | _256's_ | _16's_ | _1's_ |
| -------: | ------: | -----: | ----: |
|        0 |       0 |      1 |     0 |

This can be written down with the `0x` prefix as `0x10`. You can also fill in the zeroes to the left, such as `0x0010`, but it still means _sixteen_.

### Binary

Binary notation uses two different digits, `0` and `1`, so `0 1` means _zero_ and _one_.

So `10` in binary means _two_.

| _8's_ | _4's_ | _2's_ | _1's_ |
| ----: | ----: | ----: | ----: |
|     0 |     0 |     1 |     0 |

This can written down with the `0b` prefix as `0b10`. You can also fill in the zeroes to the left, such as `0b0010`, but it still means _two_.

## Digit positions and bases

Decimal is another name for _base-10_, hexadecimal for _base-16_ and binary for _base-2_.

If you notice, the column position is used to calculate the overall value - it's the digit's value multiplied by the base _raised to the power of_ the position, when the positions are numbered starting at zero.

_Raised to the power of_ means a value multiplied by itself a number of times and can be written using the caret (`^`) character and then the power (which can also be called the exponent.) So _ten to the power of three_ means _10 x 10 x 10_ and can be written _10 ^ 3_. Anything to the power of 0 is always 1.

An example:

### Decimal

|     _1000's_ | _100's_ | _10's_ |  _1's_ |
| -----------: | ------: | -----: | -----: |
|            3 |       2 |      1 |      0 |
|       10 ^ 3 |  10 ^ 2 | 10 ^ 1 | 10 ^ 0 |
| 10 x 10 x 10 | 10 x 10 |     10 |      1 |
|         1000 |     100 |     10 |      1 |

### Hexadecimal

|     _4096's_ | _256's_ | _16's_ |  _1's_ |
| -----------: | ------: | -----: | -----: |
|            3 |       2 |      1 |      0 |
|       16 ^ 3 |  16 ^ 2 | 16 ^ 1 | 16 ^ 0 |
| 16 x 16 x 16 | 16 x 16 |     16 |      1 |
|         4096 |     256 |     16 |      1 |

### Binary

|     _8's_ | _4's_ | _2's_ | _1's_ |
| --------: | ----: | ----: | ----: |
|         3 |     2 |     1 |     0 |
|     2 ^ 3 | 2 ^ 2 | 2 ^ 1 | 2 ^ 0 |
| 2 x 2 x 2 | 2 x 2 |     2 |     1 |
|         8 |     4 |     2 |     1 |

So the digit, the base and the digit's position are all used to calculate the value.

An example:

The binary value `0101` means _five_, reading the digits left-to-right and then multiplying the digit by the base raised to the power of the position:

    :::text
     0         1         0         1      is 5
    (0 x 8) + (1 x 4) + (0 x 2) + (1 x 1) is 5

## Bits, bytes and nibbles

When talking about the digits `0` and `1` in relation to binary, the words _**b**inary dig**it**_ are shortened to _**bit**_. 8 _bits_ make up a _byte_ and half a byte is a _nibble_.

## Counting up and carrying over

When you count numbers the digits go up by 1 and when you run out of digits you carry over a 1 to the next column. Counting is really just repeatedly doing an add with a carry.

### Decimal

| _10's_ | _1's_ | Value |
| -----: | ----: | ----: |
|      0 |     0 |  zero |
|      0 |     1 |   one |
|      0 |     2 |   two |
|      0 |     3 | three |
|      0 |     4 |  four |
|      0 |     5 |  five |
|      0 |     6 |   six |
|      0 |     7 | seven |
|      0 |     8 | eight |
|      0 |     9 |  nine |
|      1 |     0 |   ten |

### Hexadecimal

| _16's_ | _1's_ |    Value |
| -----: | ----: | -------: |
|      0 |     0 |     zero |
|      0 |     1 |      one |
|      0 |     2 |      two |
|      0 |     3 |    three |
|      0 |     4 |     four |
|      0 |     5 |     five |
|      0 |     6 |      six |
|      0 |     7 |    seven |
|      0 |     8 |    eight |
|      0 |     9 |     nine |
|      0 |     a |      ten |
|      0 |     b |   eleven |
|      0 |     c |   twelve |
|      0 |     d | thirteen |
|      0 |     e | fourteen |
|      0 |     f |  fifteen |
|      1 |     0 |  sixteen |

You can write two hexadecimal digits to mean a byte. So `0xff` in hexadecimal is `(15 x 16) + (15 x 1) = 255` and `255` is the largest value a byte can hold. A single hexadecimal digit represents a nibble.

### Binary

| _2's_ | _1's_ | Value |
| ----: | ----: | ----: |
|     0 |     0 |  zero |
|     0 |     1 |   one |
|     1 |     0 |   two |

Binary is useful because computers can represent zeroes and ones with circuits and switches that are either on (`1`) or off (`0`).

## Unsigned and signed values

Values are _integers_ (whole numbers) and some operations work with _unsigned_ values (positive numbers) and some with _signed_ values (both positive and negative numbers.)

So signed just means being able to have negative numbers. Positive can be abbreviated to _+ve_ and negative to _-ve_.

In this document, signed values are encoded in binary in an encoding called _2's complement_, that gives special significance to the left-most bit. The left-most bit is also called the _most significant bit_ and is abbreviated to _msb_. Another name for this bit is the _sign bit_.

If you're working with signed values and using 2's complement, when the msb is `1` then it means a value is a negative number.

An example:

If we count up in binary, the bits mean something different if you're reading the binary as unsigned or signed in 2's complement. When the msb is `1`, the signed number flips to negative, and then starts counting back up to zero. If you're reading the binary as unsigned, the msb doesn't have any special effect and you keep counting up.

    :::text
    +-------------+----------+--------+
    |   binary    | unsigned | signed |
    +-------------+----------+--------+
    |    00000000 |        0 |      0 |
    |    00000001 |        1 |      1 |
    |    00000010 |        2 |      2 |
    |    00000011 |        3 |      3 |
    |    00000100 |        4 |      4 |
    |                                 |
    |    ... skipped counting ...     |
    |                                 |
    |    01111111 |      127 |    127 |
    |                                 |
    | signed is -ve when the msb is 1 |
    |                                 |
    |    10000000 |      128 |   -128 |
    |    10000001 |      129 |   -127 |
    |    10000010 |      130 |   -126 |
    |    10000011 |      131 |   -125 |
    |    10000100 |      132 |   -124 |
    |                                 |
    |    ... skipped counting ...     |
    |                                 |
    |    11111100 |      252 |     -4 |
    |    11111101 |      253 |     -3 |
    |    11111110 |      254 |     -2 |
    |    11111111 |      255 |     -1 |
    +-------------+----------+--------+

The range of values that can be represented in signed and unsigned differs - there's still 256 possible values in a byte, but the ranges differ, `-128 to 127` in signed, and `0 to 255` in unsigned.

2's complement is used because addition and subtraction works the same for both signed and unsigned values.

An example:

Addition and subtraction operations both modify a bit pattern in the same way, but the bit pattern still encodes the correct values when reading the binary as unsigned and signed values.

Addition:

    :::text
    11111110 + 1 = 11111111
         254 + 1 =      255
          -2 + 1 =       -1

Subtraction:

    :::text
    11111101 - 1 = 11111100
         253 - 1 =      252
          -3 - 1 =       -4

Due to there only being a certain number of bits, there are only certain ranges of numbers that can be represented. When operations are performed that cause unexpected situations, some flags are set in status registers.

### Unsigned carry

This situation happens when you carry over and there's no more space to carry over into and the digits wrap around. This sets the carry flag.

An example:

The result of 255 + 1 is 0, because the carry has nowhere to go.

    :::text
      11111111    255
      00000001 +    1 +
      --------   ----
    1 00000000      0

### Signed overflow

This situation happens when performing an operation with two same-signed numbers that produces a result with a different sign. This sets the overflow flag.

An example:

The result of `127 + 1` is `-128`, adding two positives becomes a negative.

    :::text
      01111111    127
      00000001 +    1 +
      --------   ----
      10000000   -128

The result of `-128 - 1` (which is the same as doing `-128 + -1`) is `127`, adding two negatives becomes a positive. This also sets the carry flag.

    :::text
      10000000   -128
      11111111 +   -1 +
      --------   ----
    1 01111111    127

## Bitwise operations

There's two types of operation. One type shuffles bits left and right; and the other type compares bits in two values to set and clear bits. The comparison operations are called boolean operations, named after a 19th-Century mathematician called George Boole.

The shuffling operations are called rotating and shifting. Both involve shuffling bits either left and right into adjacent bit positions. Shuffling is useful because when you shift left, it multiplies by two, and when you shift right it divides by two (because the bit's position is related to which power of two it is being shuffled into.)

### Rotating

When you rotate and a bit is shuffled off the end, it goes into the carry flag, a further rotate brings the bit out of the carry flag and back onto the opposite end.

An example:

Rotating left shifts the bits to the left, multiplying by 2, and into the carry.

    :::text
      01000000       64
      10000000      128
    1 00000000        0
      00000001        1

Rotating left shifts the bits to the right, dividing by 2, and into the carry.

    :::text
      00000010         2
      00000001         1
      00000000 1       0
      10000000       128

### Shifting

Just like rotating, there's a shift left and a shift right. Shifting does the same as a rotate, but the shuffled bit just falls off the end, goes in the carry flag, but doesn't come back.

There's two types of shift right, a logical shift right, and an arithmetic shift right. The arithmetic shift right is meant to be used with signed 2's complement values as it remembers the previous state of the msb (the _sign bit_ that represents if a number is negative.)

An example:

The logical shift right just moves all the bits right.

    :::text
    10000100     132
    01000010      66
    00100001      33
    00010000 1    16
    00001000       8

The arithmetic shift right moves all the bits right, but sets the msb if it was set before the shift.

    :::text
    10000100    -124
    11000010     -62
    11100001     -31
    11110000 1   -16
    11111000      -8

### Comparisons (booleans)

The boolean operations can be used when bits are being used as _flags_ that represent `true` (`1`) or `false` (`0`) values (like signalling flags being raised and lowered.)

Booleans are also used in operations that check whether conditions are `true` or `false` in order to perform jumps and loops.

The following boolean operations are supported: `AND`, `OR`, `XOR` and `NOT`.

### AND

This sets a third bit if both the other two are set.

|   A |   B | A AND B |
| --: | --: | ------: |
|   0 |   0 |       0 |
|   0 |   1 |       0 |
|   1 |   0 |       0 |
|   1 |   1 |       1 |

An example:

You can use `AND` to check if a bit is set (by comparing the result with another bit pattern of all zeroes.)

A bit pattern is also called a _mask_, which is some terminology used in art to describe a bit of card used to cover some artwork.

    :::text
    A        11111111  the bit is set
    B        00000001  check this bit
    A AND B  00000001  value not zero

    A        11111110  the bit is cleared
    B        00000001  check this bit
    A AND B  00000000  value is zero

### OR

This sets a third bit if either of the other two are set.

|   A |   B | A OR B |
| --: | --: | -----: |
|   0 |   0 |      0 |
|   0 |   1 |      1 |
|   1 |   0 |      1 |
|   1 |   1 |      1 |

An example:

You can use `OR` to set a bit using a mask.

    :::text
    A       00000000  bit is cleared
    B       00000001  set this bit
    A OR B  00000001  bit is set

### XOR

This sets a third bit if either of the others bits are set, but if they're both set it clears the bit. `XOR` stands for e**X**clusive **OR**.

|   A |   B | A XOR B |
| --: | --: | ------: |
|   0 |   0 |       0 |
|   0 |   1 |       1 |
|   1 |   0 |       1 |
|   1 |   1 |       0 |

An example:

You can use `XOR` to flip a specific bit while keeping the other bits as they were.

    :::text
    A        11000000  bit is cleared
    B        00000001  flip this bit
    A XOR B  11000001  bit is set

    A        11000001  bit is set
    B        00000001  flip this bit
    A XOR B  11000000  bit is cleared

### NOT

This flips all the bits, so those that are set are cleeared, and those that are cleared are set. Also called the _1's complement_.

|   A | NOT A |
| --: | ----: |
|   0 |     1 |
|   1 |     0 |

An example:

    :::text
    A      00000100
    NOT A  11111011

As an aside, you can get the 2's complement of a number by taking the 1's complement and adding `1`.

## Encoding values, bytes and words, addresses and endianess

Operations can work with either `bytes` or `words`. Bytes are 8 bits. Words are 2 bytes. Whether something is a byte or a word is a value's _width_ (literally how _wide_ the value is in bits.)

The operation's width can be modified by changing the width specifier in the assembly language by adding a suffix of `.b` or `.w`. If the suffix isn't specified, the width is a byte.

| signed   | width | minimum value | maximum value |
| :------- | :---- | ------------: | ------------: |
| signed   | byte  |          -128 |           127 |
| unsigned | byte  |             0 |           255 |
| signed   | word  |        -32768 |         32767 |
| unsigned | word  |             0 |         65535 |

An _address_ is a special type of _unsigned word_.

Addresses are numbered locations in memory going from `0` to `65535` (or `0x0000` to `0xffff` in hexadecimal.)

Memory is a sequence of bytes that you can imagine going either left-to-right or top-to-bottom.

Operations that include addresses always encode those addresses as words.

Words are encoded in _big-endian_ order, which means the byte that represents the highest part of the value comes first in memory, and then the byte that represents the lowest part of the value comes second in memory.

### Big-endian

In big-endian order the value `0x1234` is arranged in memory as the two bytes `0x12 0x34`.

| `0x0000` | `0x0001` | `0x0002` | `0x0003` |
| -------: | -------: | -------: | -------: |
|          |   `0x12` |   `0x34` |          |

### Little-endian

In big-endian order the value `0x1234` is arranged in memory as the two bytes `0x34 0x12`.

| `0x0000` | `0x0001` | `0x0002` | `0x0003` |
| -------: | -------: | -------: | -------: |
|          |   `0x34` |   `0x12` |          |

Big-endian order is used so it's easier for humans to read values when looking at memory.

## Addressing modes

An addressing mode defines how values are included or looked up when performing operations. The width of values is separate to an addressing mode. This document describes three addressing modes:

### Immediate

This includes a value (called a constant) along with the instruction.

### Direct

This includes a value that is an address to a memory location that holds a value. A look up in memory is performed to get the value. This is useful for modifying values at known locations in memory.

An example:

With _Direct_ addressing, the address `0xA000` is used to look up the value `0x12` (as a byte) or `0x1234` (as a word.)

    :::text
    +---------+-------+
    | Address | Value |
    +---------+-------+
    | 0xA000  | 0x12  | 0xA000 used as an address
    | 0xA001  | 0x34  |
    | 0xA002  | 0xA0  |
    | 0xA003  | 0x05  |
    | 0xA004  |       |
    | 0xA005  | 0x56  |
    | 0xA006  | 0x78  |
    +---------+-------+

### Indirect

This includes a value that is an address to a memory location that holds the first of a pair of bytes. The pair of bytes is then used as _another_ address to a memory location that holds a value. This is useful for modifying values that act as _pointers_ to other locations in memory that can change where they point to as the program runs.

An example:

With _Indirect_ addressing, the address `0xA002` is used to look up two bytes, **`0xA0`** at address `0xA002` and **`0x05`** at address `0xA003`.

These looked-up values **`0xA0`** and **`0x05`** are then combined into a value, which is used as an address again - **`0xA005`**.

The address `0xA005` is used to do a further look up for the value `0x56` (as a byte) or `0x5678` (as a word.)

    :::text
    +---------+-------+
    | Address | Value |
    +---------+-------+
    | 0xA000  | 0x12  |
    | 0xA001  | 0x34  |
    | 0xA002  | 0xA0  | <------------------------
    | 0xA003  | 0x05  | 0xA002 and 0xA003 contain
    | 0xA004  |       | 0xA005 used as an address
    | 0xA005  | 0x56  | <------------------------
    | 0xA006  | 0x78  |
    +---------+-------+

## Instruction encoding format

An instruction is made up from an _opcode_ (operation code), some metadata and some operands:

-   8 bits for the operation code (a value that represents the operation to perform)
-   2 bits for the width, `00` for byte, `01` for word
-   2 bits for the operand 1 addressing mode (`00` for Empty, `01` for Immediate, `10` for Direct, `11` for Indirect)
-   2 bits for the operand 2 addressing mode (`00` for Empty, `01` for Immediate, `10` for Direct, `11` for Indirect)
-   2 bits for the operand 3 addressing mode (`00` for Empty, `01` for Immediate, `10` for Direct, `11` for Indirect)
-   0 to 48 bits for up to three operands

| Bits         | Description               |
| ------------ | ------------------------- |
| 8            | Opcode                    |
| 2            | Width                     |
| 2            | Operand 1 Addressing Mode |
| 2            | Operand 2 Addressing Mode |
| 2            | Operand 3 Addressing Mode |
| 0 or 8 or 16 | Operand 1                 |
| 0 or 8 or 16 | Operand 2                 |
| 0 or 8 or 16 | Operand 3                 |

## Operations

Widths are represented with the following symbols:

-   `-` means not applicable (although encoded as byte)
-   `B` means byte
-   `W` means word

Addressing modes are represented with the following symbols:

-   `-` means _Empty_
-   `M` means _Immediate_
-   `D` means _Direct_
-   `I` means _Indirect_

If an operation can change a status flag, that flag is shown:

-   `N` means _Negative_
-   `V` means _Overflow_
-   `Z` means _Zero_
-   `C` means _Carry_

### HLT

Halts the interpreter.

-   Widths: `- -`
-   Status: `- - - -`

| Op1 Modes | Op2 Modes | Op3 Modes |
| :-------: | :-------: | :-------: |
|  `- - -`  |  `- - -`  |  `- - -`  |

### ADD

Performs addition with two operands and stores the result at the third address.

`Op3 = Op1 + Op2`

-   Widths: `B W`
-   Status: `N V Z C`

| Op1 Modes | Op2 Modes | Op3 Modes |
| :-------: | :-------: | :-------: |
|  `M D I`  |  `M D I`  |  `- D I`  |

### SUB

Performs subtraction with two operands and stores the result at the third address.

`Op3 = Op1 - Op2`

-   Widths: `B W`
-   Status: `N V Z C`

| Op1 Modes | Op2 Modes | Op3 Modes |
| :-------: | :-------: | :-------: |
|  `M D I`  |  `M D I`  |  `- D I`  |

### MUL

Performs multiplication with two operands and stores the result at the third address.

`Op3 = Op1 x Op2`

-   Widths: `B W`
-   Status: `N V Z C`

| Op1 Modes | Op2 Modes | Op3 Modes |
| :-------: | :-------: | :-------: |
|  `M D I`  |  `M D I`  |  `- D I`  |

### DIV

Performs division with two operands and stores the result at the third address.

`Op3 = Op1 / Op2`

-   Widths: `B W`
-   Status: `N V Z C`

| Op1 Modes | Op2 Modes | Op3 Modes |
| :-------: | :-------: | :-------: |
|  `M D I`  |  `M D I`  |  `- D I`  |

# Interpreter registers

The interpreter has some registers (extra memory) for storing certain special values.

| Symbol | Description         |
| ------ | ------------------- |
| `IP`   | Instruction pointer |
| `SP`   | Stack pointer       |
| `SF`   | Status flags        |

## Instruction pointer

This is a unsigned word that contains the address of the memory location containing the next operation to perform.

## Stack pointer

This is an unsigned word that contains the address of the memory location pointing to the head of the stack. The stack is a special area of memory used for storing values using _push_ and _pop_ operations, and is also used when _jumping_ to _subroutines_ to remember the current value of the instruction pointer before jumping.

## Status flags

These are a special set of bits used as flags that are set and cleared when operations are performed. Each flag is represented with a symbol, `N V Z C`.

| Symbol | Description |
| ------ | ----------- |
| `N`    | Negative    |
| `V`    | Overflow    |
| `Z`    | Zero        |
| `C`    | Carry       |

The flags are stored in a byte. The `N` bit flag is in the msb position.

    :::text
    N V Z C - - - -

### Negative flag

This is set when an operation results in a negative value.

### Overflow flag

This is set when an operation results in a signed overflow.

### Zero flag

This is set when an operation results in a zero value.

### Carry flag

This is set when an operation results in an unsigned carry.

# Memory Layout

_work in progress_

# Assembly Syntax

_work in progress_

# Resources

The following resources have been used in the making of this:

-   [A guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
-   [Comic Mono Font](https://dtinth.github.io/comic-mono-font/)
-   [Pixel Operator Font](https://notabug.org/HarvettFox96/ttf-pixeloperator)
-   [Easy 6502](http://skilldrick.github.io/easy6502/)
-   [The 6502 overflow flag explained](http://www.righto.com/2012/12/the-6502-overflow-flag-explained.html)

# Notes

## Calculating the overflow flag

    :::javascript
    let m = 0b10000000;
    let n = 0b11111111;
    let r = m + n;
    let s = 0b10000000; // sign bit
    let v = (m ^ result) & (n ^ result) & s;
