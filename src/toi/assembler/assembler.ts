import {
    ExistingConstantError,
    ReferenceError,
    ReferenceWidthError,
} from '../errors';
import { Writer } from '../io/writer';
import { Constant } from '../models/constant';
import { Inline } from '../models/inline';
import { Instruction } from '../models/instruction';
import { Label } from '../models/label';
import { Address, Memory, Nullable, References, Tag, Width } from '../types';
import { validate } from './grammar';
import { Parser } from './parser';

export type Range = [Address, Address];
export type Entry = [string, string];
export type Callback = (entry: Nullable<Entry>) => void;

export class Assembler {
    private writer: Writer;
    private parser = new Parser();
    private lookup = new Map<Tag, Constant>();
    private references: References = [];

    constructor(memory: Memory) {
        this.writer = new Writer(memory);
    }

    public get memory() {
        return this.writer.memory;
    }

    public get valuesWritten() {
        return this.writer.valuesWritten;
    }

    public get numberOfBytesWritten() {
        return this.writer.numberOfBytesWritten;
    }

    public reset() {
        this.writer.reset();
        this.lookup.clear();
        this.references = [];
    }

    public assemble(it: string, callback: Nullable<Callback> = null) {
        const ranges = it
            .split('\n')
            .map((it) => it.trim())
            .map((it) => this.assembleLine(it));

        this.replaceReferences();

        if (callback) {
            ranges.map(this.toEntry.bind(this)).forEach(callback);
        }

        return this;
    }

    private toEntry(range: Nullable<Range>): Nullable<Entry> {
        return range
            ? [this.formattedStart(range), this.formattedValues(range)]
            : null;
    }

    private assembleLine(value: string): Nullable<Range> {
        const statement = this.parser.parse(value);
        if (!statement) {
            return null;
        }

        const { label, constant, instruction, inline } = statement;

        if (instruction) {
            validate(value, instruction);
        }

        const start = this.numberOfBytesWritten;

        if (label) {
            this.handleLabel(label, start);
        }

        if (constant) {
            this.handleConstant(constant);
        }

        if (instruction) {
            this.handleInstruction(instruction);
        }

        if (inline) {
            this.handleInline(inline);
        }

        const end = this.numberOfBytesWritten;

        return end - start > 0 ? [start, end] : null;
    }

    private handleLabel(label: Label, address: Address) {
        this.handleConstant(
            new Constant(label.tag, Width.Word, address, label),
        );
    }

    private replaceReferences() {
        const before = this.writer.numberOfBytesWritten;
        try {
            for (const [tag, address, width] of this.references) {
                const referenced = this.lookup.get(tag);
                if (!referenced) {
                    throw new ReferenceError(tag);
                }
                this.writer.jump(address);
                if (width === Width.Byte) {
                    if (referenced.width === Width.Word) {
                        throw new ReferenceWidthError(tag);
                    }
                    this.writer.writeByte(referenced.value);
                } else {
                    this.writer.writeWord(referenced.value);
                }
            }
        } finally {
            this.writer.jump(before);
        }
    }

    private formattedStart([start]: Range) {
        return start.toString(16).padStart(4, '0');
    }

    private formattedValues([start, end]: Range) {
        return this.writer
            .values(start, end)
            .map((it) => it.toString(16).padStart(2, '0'))
            .join(' ');
    }

    private handleConstant(constant: Constant) {
        const { tag } = constant;
        const existing = this.lookup.get(tag);
        if (existing) {
            throw new ExistingConstantError(existing, constant);
        }
        this.lookup.set(tag, constant);
    }

    private handleInstruction(instruction: Instruction) {
        this.writer.writeInstruction(instruction, this.references);
    }

    private handleInline(inline: Inline) {
        if (inline.width === Width.Byte) {
            this.writer.writeBytes(inline.values);
        }
        if (inline.width === Width.Word) {
            this.writer.writeWords(inline.values);
        }
    }
}
