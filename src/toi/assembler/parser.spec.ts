import { UnknownOperationError, UnrepresentableValueError } from '../errors';
import { Constant } from '../models/constant';
import { Inline, InlineEncodingError } from '../models/inline';
import { Instruction } from '../models/instruction';
import { Label } from '../models/label';
import { Mode, Operation, Tag, Value, Width } from '../types';
import { Parser } from './parser';

type Case = [string, Operation, Width, [Mode, Value, Tag][]];

const zeroed = [Mode.Direct, 0, null];
const immediate = [Mode.Immediate, 1, null];
const immediateTagged = [Mode.Immediate, 0, 'a'];
const direct = [Mode.Direct, 1, null];
const directTagged = [Mode.Direct, 0, 'a'];
const indirect = [Mode.Indirect, 1, null];
const indirectTagged = [Mode.Indirect, 0, 'a'];
const empty = [Mode.Empty, 0, null];

const parse = (value: string) => new Parser().parse(value);

describe('parser', () => {
    describe('parsing instructions', () => {
        it.each([
            // operand 1 immediate
            [
                'add 1 @0 @0',
                Operation.ADD,
                Width.Byte,
                [immediate, zeroed, zeroed],
            ],
            [
                'add 0b1 @0 @0',
                Operation.ADD,
                Width.Byte,
                [immediate, zeroed, zeroed],
            ],
            [
                'add 0x1 @0 @0',
                Operation.ADD,
                Width.Byte,
                [immediate, zeroed, zeroed],
            ],
            [
                'add a @0 @0',
                Operation.ADD,
                Width.Byte,
                [immediateTagged, zeroed, zeroed],
            ],
            // operand 1 direct
            [
                'add @1 @0 @0',
                Operation.ADD,
                Width.Byte,
                [direct, zeroed, zeroed],
            ],
            [
                'add @0b1 @0 @0',
                Operation.ADD,
                Width.Byte,
                [direct, zeroed, zeroed],
            ],
            [
                'add @0x1 @0 @0',
                Operation.ADD,
                Width.Byte,
                [direct, zeroed, zeroed],
            ],
            [
                'add @a @0 @0',
                Operation.ADD,
                Width.Byte,
                [directTagged, zeroed, zeroed],
            ],
            // operand 1 indrect
            [
                'add @@1 @0 @0',
                Operation.ADD,
                Width.Byte,
                [indirect, zeroed, zeroed],
            ],
            [
                'add @@0b1 @0 @0',
                Operation.ADD,
                Width.Byte,
                [indirect, zeroed, zeroed],
            ],
            [
                'add @@0x1 @0 @0',
                Operation.ADD,
                Width.Byte,
                [indirect, zeroed, zeroed],
            ],
            [
                'add @@a @0 @0',
                Operation.ADD,
                Width.Byte,
                [indirectTagged, zeroed, zeroed],
            ],
            // operand 2 immediate
            [
                'add @0 1 @0',
                Operation.ADD,
                Width.Byte,
                [zeroed, immediate, zeroed],
            ],
            [
                'add @0 0b1 @0',
                Operation.ADD,
                Width.Byte,
                [zeroed, immediate, zeroed],
            ],
            [
                'add @0 0x1 @0',
                Operation.ADD,
                Width.Byte,
                [zeroed, immediate, zeroed],
            ],
            [
                'add @0 a @0',
                Operation.ADD,
                Width.Byte,
                [zeroed, immediateTagged, zeroed],
            ],
            // operand 2 direct
            [
                'add @0 @1 @0',
                Operation.ADD,
                Width.Byte,
                [zeroed, direct, zeroed],
            ],
            [
                'add @0 @0b1 @0',
                Operation.ADD,
                Width.Byte,
                [zeroed, direct, zeroed],
            ],
            [
                'add @0 @0x1 @0',
                Operation.ADD,
                Width.Byte,
                [zeroed, direct, zeroed],
            ],
            [
                'add @0 @a @0',
                Operation.ADD,
                Width.Byte,
                [zeroed, directTagged, zeroed],
            ],
            // operand 2 indirect
            [
                'add @0 @@1 @0',
                Operation.ADD,
                Width.Byte,
                [zeroed, indirect, zeroed],
            ],
            [
                'add @0 @@0b1 @0',
                Operation.ADD,
                Width.Byte,
                [zeroed, indirect, zeroed],
            ],
            [
                'add @0 @@0x1 @0',
                Operation.ADD,
                Width.Byte,
                [zeroed, indirect, zeroed],
            ],
            [
                'add @0 @@a @0',
                Operation.ADD,
                Width.Byte,
                [zeroed, indirectTagged, zeroed],
            ],
            // operand 3 immediate
            [
                'add @0 @0 1',
                Operation.ADD,
                Width.Byte,
                [zeroed, zeroed, immediate],
            ],
            [
                'add @0 @0 0b1',
                Operation.ADD,
                Width.Byte,
                [zeroed, zeroed, immediate],
            ],
            [
                'add @0 @0 0x1',
                Operation.ADD,
                Width.Byte,
                [zeroed, zeroed, immediate],
            ],
            [
                'add @0 @0 a',
                Operation.ADD,
                Width.Byte,
                [zeroed, zeroed, immediateTagged],
            ],
            // operand 3 direct
            [
                'add @0 @0 @1',
                Operation.ADD,
                Width.Byte,
                [zeroed, zeroed, direct],
            ],
            [
                'add @0 @0 @0b1',
                Operation.ADD,
                Width.Byte,
                [zeroed, zeroed, direct],
            ],
            [
                'add @0 @0 @0x1',
                Operation.ADD,
                Width.Byte,
                [zeroed, zeroed, direct],
            ],
            [
                'add @0 @0 @a',
                Operation.ADD,
                Width.Byte,
                [zeroed, zeroed, directTagged],
            ],
            // operand 3 indirect
            [
                'add @0 @0 @@1',
                Operation.ADD,
                Width.Byte,
                [zeroed, zeroed, indirect],
            ],
            [
                'add @0 @0 @@0b1',
                Operation.ADD,
                Width.Byte,
                [zeroed, zeroed, indirect],
            ],
            [
                'add @0 @0 @@0x1',
                Operation.ADD,
                Width.Byte,
                [zeroed, zeroed, indirect],
            ],
            [
                'add @0 @0 @@a',
                Operation.ADD,
                Width.Byte,
                [zeroed, zeroed, indirectTagged],
            ],
            // negative decimals
            [
                'add -1 @0 @0',
                Operation.ADD,
                Width.Byte,
                [[Mode.Immediate, -1, null], zeroed, zeroed],
            ],
            [
                'add @0 -1 @0',
                Operation.ADD,
                Width.Byte,
                [zeroed, [Mode.Immediate, -1, null], zeroed],
            ],
            // no operands
            ['hlt', Operation.HLT, Width.Byte, [empty, empty, empty]],
        ] as Case[])(
            'should parse instruction "%s"',
            (
                string,
                operation,
                width,
                [
                    [mode1, value1, tag1],
                    [mode2, value2, tag2],
                    [mode3, value3, tag3],
                ],
            ) => {
                const it = parse(string);
                expect(it?.label).toBeNull();
                expect(it?.value).toBeInstanceOf(Instruction);
                expect(it?.value).toMatchObject({
                    operation,
                    width,
                    operands: [
                        { mode: mode1, value: value1, tag: tag1 },
                        { mode: mode2, value: value2, tag: tag2 },
                        { mode: mode3, value: value3, tag: tag3 },
                    ],
                });
            },
        );

        it('should throw an error with an invalid instruction', () => {
            expect(() => parse('xxx')).toThrow(UnknownOperationError);
        });

        it('should parse word widths', () => {
            expect(parse('add.w 1 2 @@3')?.value).toMatchObject({
                operation: Operation.ADD,
                width: Width.Word,
                operands: [
                    { mode: Mode.Immediate, value: 1, tag: null },
                    { mode: Mode.Immediate, value: 2, tag: null },
                    { mode: Mode.Indirect, value: 3, tag: null },
                ],
            });
        });
    });

    describe('parsing labels', () => {
        it('should parse a standalone label', () => {
            const statement = 'a:';
            expect(parse(statement)?.label).toEqual(new Label('a'));
            expect(parse(statement)?.value).toBeNull();
        });

        it('should parse a label on an instruction', () => {
            const statement = 'b: add 1 2 @3';
            expect(parse(statement)?.label).toEqual(new Label('b'));
            expect(parse(statement)?.value).toMatchObject({
                operation: Operation.ADD,
                width: Width.Byte,
                operands: [
                    { mode: Mode.Immediate, value: 1, tag: null },
                    { mode: Mode.Immediate, value: 2, tag: null },
                    { mode: Mode.Direct, value: 3, tag: null },
                ],
            });
        });

        it('should parse a label on inline bytes', () => {
            const statement = 'c: .b 1';
            expect(parse(statement)?.label).toEqual(new Label('c'));
            expect(parse(statement)?.value).toMatchObject({
                width: Width.Byte,
                values: [1],
            });
        });

        it('should parse a label on inline words', () => {
            const statement = 'd: .w 2';
            expect(parse(statement)?.label).toEqual(new Label('d'));
            expect(parse(statement)?.value).toMatchObject({
                width: Width.Word,
                values: [2],
            });
        });

        it('should parse a label on inline characters', () => {
            const statement = 'e: .b `A`';
            expect(parse(statement)?.label).toEqual(new Label('e'));
            expect(parse(statement)?.value).toMatchObject({
                width: Width.Byte,
                values: [0x41, 0x00],
            });
        });
    });

    describe('parsing constants', () => {
        it('should parse constants', () => {
            const expected = new Constant('a', Width.Byte, 1);
            expect(parse('a = 1')?.value).toEqual(expected);
            expect(parse('a=1')?.value).toEqual(expected);
            expect(parse('a = 0b1')?.value).toEqual(expected);
            expect(parse('a=0b1')?.value).toEqual(expected);
            expect(parse('a = 0x1')?.value).toEqual(expected);
            expect(parse('a=0x1')?.value).toEqual(expected);
        });

        it('should infer bytes', () => {
            expect(parse('a = 0')?.value).toEqual(
                new Constant('a', Width.Byte, 0),
            );
            expect(parse('a = -128')?.value).toEqual(
                new Constant('a', Width.Byte, -128),
            );
            expect(parse('a = 255')?.value).toEqual(
                new Constant('a', Width.Byte, 255),
            );
        });

        it('should infer words', () => {
            expect(parse('a = -32768')?.value).toEqual(
                new Constant('a', Width.Word, -32768),
            );
            expect(parse('a = 65535')?.value).toEqual(
                new Constant('a', Width.Word, 65535),
            );
        });

        it('should throw an error for unrepresentable words', () => {
            expect(() => parse('a = -32769')).toThrow(
                UnrepresentableValueError,
            );
            expect(() => parse('a = 65536')).toThrow(UnrepresentableValueError);
        });

        it('should throw an error for unrepresentable bytes', () => {
            expect(() => parse('add.b 1 0xffff @2')).toThrow(
                UnrepresentableValueError,
            );
        });
    });

    describe('parsing inlined data', () => {
        it('should parse bytes', () => {
            const expected = new Inline(Width.Byte, [1, 2, 3]);
            expect(parse('.b 1 0b10 0x03')?.value).toEqual(expected);
        });

        it('should parse words', () => {
            const expected = new Inline(Width.Word, [5, 6, 7]);
            expect(parse('.w 5 0b110 0x07')?.value).toEqual(expected);
        });

        it('should parse null-terminated byte strings', () => {
            const expected = new Inline(Width.Byte, [0x41, 0x42, 0x43, 0x00]);
            expect(parse('.b `ABC`')?.value).toEqual(expected);
        });

        it('should parse null-terminated word strings', () => {
            const expected = new Inline(Width.Word, [0x41, 0x42, 0x43, 0x00]);
            expect(parse('.w `ABC`')?.value).toEqual(expected);
        });

        it('should throw an error with non-ascii characters when encoding bytes', () => {
            expect(() => parse('.b `あ`')).toThrow(InlineEncodingError);
        });

        it('should parse non-ascii characters when encoding words', () => {
            const expected = new Inline(Width.Word, [12354, 0x00]);
            expect(parse('.w `あ`')?.value).toEqual(expected);
        });
    });
});
