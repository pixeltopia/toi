import { Instruction } from '../models/instruction';
import { Mode, Operation, Width } from '../types';
import { validate } from './grammar';

const makeLine = (
    operation: Operation,
    width: Width,
    mode1: Mode,
    mode2: Mode,
    mode3: Mode,
) =>
    `operation: ${operation}, width: ${width}, mode1: ${mode1}, mode2: ${mode2}, mode3: ${mode3}`;

const check = (
    operation: Operation,
    widths: Width[],
    modes1: Mode[] = [Mode.Empty],
    modes2: Mode[] = [Mode.Empty],
    modes3: Mode[] = [Mode.Empty],
) => {
    for (const width of widths) {
        for (const mode1 of modes1) {
            for (const mode2 of modes2) {
                for (const mode3 of modes3) {
                    const instruction = new Instruction();
                    instruction.init(operation, width);
                    instruction.set(0, mode1, 0);
                    instruction.set(1, mode2, 0);
                    instruction.set(2, mode3, 0);
                    expect(() =>
                        validate(
                            makeLine(operation, width, mode1, mode2, mode3),
                            instruction,
                        ),
                    ).not.toThrow();
                }
            }
        }
    }
};

const b = [Width.Byte];
const bw = [...b, Width.Word];

const imm = [Mode.Immediate];
const mem = [Mode.Direct, Mode.Indirect];
const all = [...imm, ...mem];

describe('grammar', () => {
    it('should validate instructions', () => {
        check(Operation.HLT, b);
        check(Operation.BRA, b, mem);
        check(Operation.BEQ, bw, all, all, mem);
        check(Operation.BNE, bw, all, all, mem);
        check(Operation.BLT, bw, all, all, mem);
        check(Operation.BGT, bw, all, all, mem);
        check(Operation.BLE, bw, all, all, mem);
        check(Operation.BGE, bw, all, all, mem);
        check(Operation.BLTU, bw, all, all, mem);
        check(Operation.BGTU, bw, all, all, mem);
        check(Operation.BLEU, bw, all, all, mem);
        check(Operation.BGEU, bw, all, all, mem);
        check(Operation.SLT, bw, all, all, mem);
        check(Operation.SGT, bw, all, all, mem);
        check(Operation.SLTU, bw, all, all, mem);
        check(Operation.SGTU, bw, all, all, mem);
        check(Operation.OR, bw, all, all, mem);
        check(Operation.AND, bw, all, all, mem);
        check(Operation.XOR, bw, all, all, mem);
        check(Operation.NOT, bw, all, mem);
        check(Operation.NEG, bw, all, mem);
        check(Operation.ROL, bw, all, mem);
        check(Operation.ROR, bw, all, mem);
        check(Operation.LSL, bw, all, mem);
        check(Operation.LSR, bw, all, mem);
        check(Operation.ASR, bw, all, mem);
        check(Operation.EXT, bw, all, mem);
        check(Operation.ADD, bw, all, all, mem);
        check(Operation.SUB, bw, all, all, mem);
        check(Operation.MOV, bw, all, mem);
        check(Operation.PSH, bw, all);
        check(Operation.POP, bw, mem);
        check(Operation.JTS, b, mem);
        check(Operation.RTS, b);
        check(Operation.PSHM, b, mem, imm);
        check(Operation.POPM, b, mem, imm);
        check(Operation.MOVM, bw, mem, mem, imm);
    });
});
