import {
    NaNError,
    UnknownOperationError,
    UnrepresentableValueError,
} from '../errors';
import { Constant } from '../models/constant';
import { Inline } from '../models/inline';
import { Instruction } from '../models/instruction';
import { Label } from '../models/label';
import { Statement } from '../models/statement';
import { Mode, Nullable, Operation, Width } from '../types';

export class Parser {
    public parse(statement: string): Nullable<Statement> {
        return (
            this.parseInstruction(statement) ||
            this.parseLabel(statement) ||
            this.parseConstant(statement) ||
            this.parseInlineBytesOrWords(statement) ||
            this.parseInlineChars(statement)
        );
    }

    private parseInstruction(statement: string): Nullable<Statement> {
        const matched = this.matchInstruction(statement);
        if (!matched) {
            return null;
        }
        const [
            ,
            tokenLabel,
            tokenOperation,
            tokenWidth,
            tokenMode1,
            tokenOperand1,
            tokenMode2,
            tokenOperand2,
            tokenMode3,
            tokenOperand3,
        ] = matched;

        const label = this.makeOptionalLabel(tokenLabel);

        const operation = this.makeOperation(tokenOperation);
        if (operation === undefined) {
            throw new UnknownOperationError(tokenOperation);
        }

        const width = this.makeWidth(tokenWidth);

        const instruction = new Instruction(operation, width);

        const mode1 = this.makeMode(tokenMode1, tokenOperand1);
        const mode2 = this.makeMode(tokenMode2, tokenOperand2);
        const mode3 = this.makeMode(tokenMode3, tokenOperand3);

        const width1 = this.widthForMode(mode1, width);
        const width2 = this.widthForMode(mode2, width);
        const width3 = this.widthForMode(mode3, width);

        instruction.set(
            0,
            mode1,
            this.makeValue(tokenOperand1, width1),
            this.makeTag(tokenOperand1),
        );
        instruction.set(
            1,
            mode2,
            this.makeValue(tokenOperand2, width2),
            this.makeTag(tokenOperand2),
        );
        instruction.set(
            2,
            mode3,
            this.makeValue(tokenOperand3, width3),
            this.makeTag(tokenOperand3),
        );

        return new Statement(label, instruction);
    }

    private widthForMode(mode: Mode, width: Width) {
        switch (mode) {
            case Mode.Direct:
            case Mode.Indirect:
                return Width.Word;
            case Mode.Immediate:
                return width;
            default:
                return Width.Byte;
        }
    }

    private makeOptionalLabel(value: string | undefined) {
        return !!value ? new Label(value) : null;
    }

    private matchInstruction(statement: string) {
        const operation = '([a-z]{3})';
        const width = `(?:${this.widthRegex})?`;
        const mode = '@{0,2}';
        const operand = `(?:\\s+(${mode})(${this.constantRegex}|${this.numberRegex}))?`;
        return new RegExp(
            `^${this.labelRegex}\\s*${operation}${width}${operand}${operand}${operand}$`,
            'i',
        ).exec(statement);
    }

    private makeOperation(value: string) {
        return {
            hlt: Operation.HLT,
            add: Operation.ADD,
            sub: Operation.SUB,
            mov: Operation.MOV,
        }[value.toLowerCase()];
    }

    private makeWidth(value: string) {
        return value && value.toLowerCase() === 'w' ? Width.Word : Width.Byte;
    }

    private makeMode(mode: string, operand: string) {
        if (!operand) {
            return Mode.Empty;
        }
        switch (mode) {
            case '@':
                return Mode.Direct;
            case '@@':
                return Mode.Indirect;
            default:
                return Mode.Immediate;
        }
    }

    private makeValue(value: string, width: Nullable<Width> = null) {
        let result = 0;

        const tagReferencePlaceholderValue = 0;

        if (value) {
            if (this.isTag(value)) {
                result = tagReferencePlaceholderValue;
            } else {
                const lower = value.toLowerCase();
                if (lower.startsWith('0b')) {
                    result = Number.parseInt(lower.substring(2), 2);
                } else if (value.startsWith('0x')) {
                    result = Number.parseInt(lower.substring(2), 16);
                } else {
                    result = Number.parseInt(lower);
                }
            }
            if (Number.isNaN(result)) {
                this.throwNaNError(value);
            }
        }

        if (width !== null) {
            const inferred = this.inferWidth(result);
            if (width !== inferred) {
                if (width === Width.Byte && inferred === Width.Word) {
                    this.throwByteError(value);
                }
                if (width === Width.Word && inferred === Width.Byte) {
                    // widening type cast
                }
            }
        }

        return result;
    }

    private throwByteError(value: string | number): never {
        throw new UnrepresentableValueError(value, Width.Byte);
    }

    private throwWordError(value: string | number): never {
        throw new UnrepresentableValueError(value, Width.Word);
    }

    private throwNaNError(value: string): never {
        throw new NaNError(value);
    }

    private makeTag(value: string) {
        return this.isTag(value) ? value : null;
    }

    private isTag(value: string) {
        return !!value && !!value.match(/^[a-z]+$/i);
    }

    private parseLabel(statement: string): Nullable<Statement> {
        const matched = this.matchLabel(statement);
        if (!matched) {
            return null;
        }
        const [, tokenTag] = matched;
        return new Statement(new Label(tokenTag));
    }

    private matchLabel(value: string) {
        return /^([a-z]+):$/i.exec(value);
    }

    private parseConstant(statement: string): Nullable<Statement> {
        const matched = this.matchConstant(statement);
        if (!matched) {
            return null;
        }
        const [, tokenLabel, tokenTag, tokenValue] = matched;

        const label = this.makeOptionalLabel(tokenLabel);
        const value = this.makeValue(tokenValue);
        const width = this.inferWidth(value);

        return new Statement(label, new Constant(tokenTag, width, value));
    }

    private matchConstant(statement: string) {
        return new RegExp(
            `^${this.labelRegex}\\s*(${this.constantRegex})\\s*=\\s*(${this.numberRegex})$`,
            'i',
        ).exec(statement);
    }

    private inferWidth(value: number) {
        const byteSized =
            (0 <= value && value <= 255) || (-128 <= value && value <= 127);

        const wordSized =
            (0 <= value && value <= 65535) ||
            (-32768 <= value && value <= 32767);

        if (byteSized) {
            return Width.Byte;
        }

        if (wordSized) {
            return Width.Word;
        }

        this.throwWordError(value);
    }

    private parseInlineBytesOrWords(statement: string): Nullable<Statement> {
        const matched = this.matchInlineBytesOrWords(statement);
        if (!matched) {
            return null;
        }

        const [, tokenLabel, tokenWidth] = matched;

        const label = this.makeOptionalLabel(tokenLabel);
        const width = this.makeWidth(tokenWidth);

        const tokenValues = this.matchAllValues(statement);
        if (!tokenValues) {
            return null;
        }

        const values = [...tokenValues].map(([, value]) =>
            this.makeValue(value, width),
        );

        return new Statement(label, new Inline(width, values));
    }

    private matchInlineBytesOrWords(statement: string) {
        return new RegExp(
            `^${this.labelRegex}(?:\\s*${this.widthRegex})(?:\\s*(${this.numberRegex}))+?`,
            'i',
        ).exec(statement);
    }

    private matchAllValues(value: string) {
        return value.matchAll(
            new RegExp(`(?:\\s*(${this.numberRegex}))`, 'ig'),
        );
    }

    private parseInlineChars(statement: string): Nullable<Statement> {
        const matched = this.matchInlineChars(statement);
        if (!matched) {
            return null;
        }

        const [, tokenLabel, tokenWidth, tokenChars] = matched;

        const label = this.makeOptionalLabel(tokenLabel);
        const width = this.makeWidth(tokenWidth);

        return new Statement(
            label,
            width === Width.Byte
                ? Inline.makeBytes(tokenChars)
                : Inline.makeWords(tokenChars),
        );
    }

    private matchInlineChars(statement: string) {
        return new RegExp(
            `^${this.labelRegex}\\s*${this.widthRegex}(?:\\s*\`(.*)\`)$`,
            'i',
        ).exec(statement);
    }

    private get labelRegex() {
        return '(?:\\s*([a-z]+):)?';
    }

    private get numberRegex() {
        return '0x[a-f|0-9]{1,4}|0b[01]{1,16}|-?[0-9]+';
    }

    private get widthRegex() {
        return '\\.([b|w])';
    }

    private get constantRegex() {
        return '[a-z]+';
    }
}
