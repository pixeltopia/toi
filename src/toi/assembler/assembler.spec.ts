import {
    ExistingConstantError,
    ReferenceError,
    ReferenceWidthError,
} from '../errors';
import { makeInstruction, makeWord } from '../test-helpers';
import { Mode, Nullable, Operation, Width } from '../types';
import { Assembler, Callback, Entry } from './assembler';

const assemble = (lines: string, callback?: Nullable<Callback>) =>
    new Assembler(new Uint8Array(20)).assemble(lines, callback).valuesWritten;

describe('assembler', () => {
    describe('calling back and assembling values', () => {
        const lines = `
            a = 3
            main:
                add.b 1 2 @a
                hlt
            bytes: .b 1 2 3 4 5 6
            words: .w 0xffff`;

        it('should callback with formatted entries', () => {
            const entries: Nullable<Entry>[] = [];

            assemble(lines, (entry) => {
                entries.push(entry);
            });

            const [hi, lo] = makeInstruction(
                Operation.ADD,
                Width.Byte,
                Mode.Immediate,
                Mode.Immediate,
                Mode.Direct,
            );

            const hiStr = hi.toString(16).padStart(2, '0');
            const loStr = lo.toString(16).padStart(2, '0');

            expect(entries).toEqual([
                null,
                null,
                null,
                ['0000', `${hiStr} ${loStr} 01 02 00 03`],
                ['0006', '00 00'],
                ['0008', '01 02 03 04 05 06'],
                ['000e', 'ff ff'],
            ]);
        });

        it('should assemble instructions and inline data', () => {
            expect(assemble(lines)).toEqual([
                ...makeInstruction(
                    Operation.ADD,
                    Width.Byte,
                    Mode.Immediate,
                    Mode.Immediate,
                    Mode.Direct,
                ),
                1,
                2,
                ...makeWord(3),
                ...makeInstruction(Operation.HLT),
                1,
                2,
                3,
                4,
                5,
                6,
                ...makeWord(0xffff),
            ]);
        });
    });

    describe('substituting constants', () => {
        it('should substitute constants that appear before instructions', () => {
            const lines = `
                a = 3
                add.b 1 2 @a`;
            expect(assemble(lines)).toEqual([
                ...makeInstruction(
                    Operation.ADD,
                    Width.Byte,
                    Mode.Immediate,
                    Mode.Immediate,
                    Mode.Direct,
                ),
                1,
                2,
                ...makeWord(3),
            ]);
        });

        it('should substitute constants that appear after instructions', () => {
            const lines = `
                add.b 1 2 @a
                a = 3`;
            expect(assemble(lines)).toEqual([
                ...makeInstruction(
                    Operation.ADD,
                    Width.Byte,
                    Mode.Immediate,
                    Mode.Immediate,
                    Mode.Direct,
                ),
                1,
                2,
                ...makeWord(3),
            ]);
        });

        it('should throw an error when the constant reference is missing', () => {
            expect(() => assemble('add.b 1 2 @a')).toThrow(ReferenceError);
        });

        it('should throw an error when a constant is too wide to fit', () => {
            const lines = `
                word = 0xfffe
                add.b 1 word @0xffff`;
            expect(() => assemble(lines)).toThrow(ReferenceWidthError);
        });

        it('should handle duplicate constants', () => {
            const lines = `
                a = 0x03
                add.w 1 2 @3
                a = 0x04`;
            expect(() => assemble(lines)).toThrow(ExistingConstantError);
        });
    });

    describe('widening values', () => {
        it('should widen immediate values and constants', () => {
            const lines = `
                a = 0x03
                add.w 1 2 @a`;
            expect(assemble(lines)).toEqual([
                ...makeInstruction(
                    Operation.ADD,
                    Width.Word,
                    Mode.Immediate,
                    Mode.Immediate,
                    Mode.Direct,
                ),
                ...makeWord(1),
                ...makeWord(2),
                ...makeWord(3),
            ]);
        });
    });

    describe('substituting labels', () => {
        it('should substitute labels with their addresses', () => {
            const lines = `
                add 1 @label @3
                label: .b 0xff`;

            const sizeOfOperation = 1;
            const sizeOfMetadata = 1;
            const sizeOfImmediate = 1;
            const sizeOfDirect = 2;

            const sizeOfInstruction =
                sizeOfOperation +
                sizeOfMetadata +
                sizeOfImmediate +
                sizeOfDirect +
                sizeOfDirect;

            expect(assemble(lines)).toEqual([
                ...makeInstruction(
                    Operation.ADD,
                    Width.Byte,
                    Mode.Immediate,
                    Mode.Direct,
                    Mode.Direct,
                ),
                1,
                ...makeWord(sizeOfInstruction),
                ...makeWord(3),
                0xff,
            ]);
        });

        it('should substitute labels that appear before references', () => {
            const lines = 'label: add 1 @label @3';
            const labeled = 0;
            expect(assemble(lines)).toEqual([
                ...makeInstruction(
                    Operation.ADD,
                    Width.Byte,
                    Mode.Immediate,
                    Mode.Direct,
                    Mode.Direct,
                ),
                1,
                ...makeWord(labeled),
                ...makeWord(3),
            ]);
        });

        it('should handle duplicate labels', () => {
            const lines = `
                abc:
                abc:`;
            expect(() => assemble(lines)).toThrow(ExistingConstantError);
        });
    });

    describe('checking duplicate constants and labels', () => {
        it('should handle duplicate constant and label', () => {
            const lines = `
                abc = 1
                abc:`;
            expect(() => assemble(lines)).toThrow(ExistingConstantError);
        });

        it('should handle duplicate label and constant', () => {
            const lines = `
                abc:
                abc = 1`;
            expect(() => assemble(lines)).toThrow(ExistingConstantError);
        });
    });
});
