import { InstructionFormatError } from '../errors';
import { Instruction } from '../models/instruction';
import { Mode, Operation, Width } from '../types';

class Rule {
    constructor(
        private operation: Operation,
        private widths: Width[] = [],
        private modes1: Mode[] = [],
        private modes2: Mode[] = [],
        private modes3: Mode[] = [],
    ) {}

    public matches({
        operation,
        width,
        operands: [{ mode: mode1 }, { mode: mode2 }, { mode: mode3 }],
    }: Instruction) {
        return (
            this.operation === operation &&
            this.widths.includes(width) &&
            this.check(this.modes1, mode1) &&
            this.check(this.modes2, mode2) &&
            this.check(this.modes3, mode3)
        );
    }

    private check(modes: Mode[], mode: Mode) {
        return (
            (modes.length === 0 && mode === Mode.Empty) || modes.includes(mode)
        );
    }
}

const b = [Width.Byte];
const bw = [...b, Width.Word];

const imm = [Mode.Immediate];
const mem = [Mode.Direct, Mode.Indirect];
const all = [...imm, ...mem];

const grammar = [
    new Rule(Operation.HLT, b),
    new Rule(Operation.BRA, b, mem),
    new Rule(Operation.BEQ, bw, all, all, mem),
    new Rule(Operation.BNE, bw, all, all, mem),
    new Rule(Operation.BLT, bw, all, all, mem),
    new Rule(Operation.BGT, bw, all, all, mem),
    new Rule(Operation.BLE, bw, all, all, mem),
    new Rule(Operation.BGE, bw, all, all, mem),
    new Rule(Operation.BLTU, bw, all, all, mem),
    new Rule(Operation.BGTU, bw, all, all, mem),
    new Rule(Operation.BLEU, bw, all, all, mem),
    new Rule(Operation.BGEU, bw, all, all, mem),
    new Rule(Operation.SLT, bw, all, all, mem),
    new Rule(Operation.SGT, bw, all, all, mem),
    new Rule(Operation.SLTU, bw, all, all, mem),
    new Rule(Operation.SGTU, bw, all, all, mem),
    new Rule(Operation.OR, bw, all, all, mem),
    new Rule(Operation.AND, bw, all, all, mem),
    new Rule(Operation.XOR, bw, all, all, mem),
    new Rule(Operation.NOT, bw, all, mem),
    new Rule(Operation.NEG, bw, all, mem),
    new Rule(Operation.ROL, bw, all, mem),
    new Rule(Operation.ROR, bw, all, mem),
    new Rule(Operation.LSL, bw, all, mem),
    new Rule(Operation.LSR, bw, all, mem),
    new Rule(Operation.ASR, bw, all, mem),
    new Rule(Operation.EXT, bw, all, mem),
    new Rule(Operation.ADD, bw, all, all, mem),
    new Rule(Operation.SUB, bw, all, all, mem),
    new Rule(Operation.MOV, bw, all, mem),
    new Rule(Operation.PSH, bw, all),
    new Rule(Operation.POP, bw, mem),
    new Rule(Operation.JTS, b, mem),
    new Rule(Operation.RTS, b),
    new Rule(Operation.PSHM, b, mem, imm),
    new Rule(Operation.POPM, b, mem, imm),
    new Rule(Operation.MOVM, bw, mem, mem, imm),
];

export const validate = (line: string, instruction: Instruction) => {
    if (!grammar.find((rule) => rule.matches(instruction))) {
        throw new InstructionFormatError(line, instruction);
    }
};
