import { Width } from '../types';
import { Inline, InlineEncodingError } from './inline';

describe('inline', () => {
    it('should have a width and some values', () => {
        const bytes = new Inline(Width.Byte, [1, 2, 3, 4]);
        expect(bytes.width).toEqual(Width.Byte);
        expect(bytes.values).toEqual([1, 2, 3, 4]);

        const words = new Inline(Width.Word, [1, 2, 3, 4]);
        expect(words.width).toEqual(Width.Word);
        expect(words.values).toEqual([1, 2, 3, 4]);
    });

    describe('encoding strings', () => {
        it('should make a null-terminated ascii byte string', () => {
            const bytes = Inline.makeBytes('A');
            expect(bytes.width).toEqual(Width.Byte);
            expect(bytes.values).toEqual([0x41, 0x00]);
        });

        it('should make an ascii byte string without a null terminator', () => {
            expect(Inline.makeBytes('A', false).values).toEqual([0x41]);
        });

        it('should throw an error with a non-ascii character', () => {
            expect(() => Inline.makeBytes('あ')).toThrow(InlineEncodingError);
        });

        it('should make a null-terminated word string', () => {
            const words = Inline.makeWords('あ');
            expect(words.width).toEqual(Width.Word);
            expect(words.values).toEqual([12354, 0x00]);
        });

        it('should make a word string without a null terminator', () => {
            expect(Inline.makeWords('あ', false).values).toEqual([12354]);
        });
    });
});
