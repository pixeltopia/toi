import { Mode, Operation, Width } from '../types';
import { Instruction } from './instruction';

describe('instruction', () => {
    const cleared = {
        operation: Operation.HLT,
        width: Width.Byte,
        operands: [
            { mode: Mode.Empty, value: 0 },
            { mode: Mode.Empty, value: 0 },
            { mode: Mode.Empty, value: 0 },
        ],
    };

    it('should be cleared on construction', () => {
        expect(new Instruction()).toMatchObject(cleared);
    });

    it('should clear', () => {
        const instruction = new Instruction();

        instruction.init(Operation.ADD, Width.Word);
        instruction.set(0, Mode.Immediate, 1);
        instruction.set(1, Mode.Immediate, 2);
        instruction.set(2, Mode.Direct, 0x8000);

        instruction.clear();

        expect(instruction).toMatchObject(cleared);
    });

    it('should be initialised', () => {
        const instruction = new Instruction();

        instruction.init(Operation.ADD, Width.Word);
        instruction.set(0, Mode.Immediate, 1);
        instruction.set(1, Mode.Immediate, 2);
        instruction.set(2, Mode.Direct, 0x8000);

        expect(instruction).toMatchObject({
            operation: Operation.ADD,
            width: Width.Word,
            operands: [
                { mode: Mode.Immediate, value: 1 },
                { mode: Mode.Immediate, value: 2 },
                { mode: Mode.Direct, value: 0x8000 },
            ],
        });
    });
});
