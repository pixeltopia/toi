import { Nullable } from '../types';
import { Constant } from './constant';
import { Inline } from './inline';
import { Instruction } from './instruction';
import { Label } from './label';

export class Statement {
    constructor(
        public readonly label: Nullable<Label> = null,
        public readonly value: Nullable<Instruction | Constant | Inline> = null,
    ) {}

    public get instruction(): Nullable<Instruction> {
        return this.value instanceof Instruction ? this.value : null;
    }

    public get constant(): Nullable<Constant> {
        return this.value instanceof Constant ? this.value : null;
    }

    public get inline(): Nullable<Inline> {
        return this.value instanceof Inline ? this.value : null;
    }
}
