import {
    Address,
    Mode,
    Nullable,
    Operation,
    Tag,
    Value,
    Width,
} from '../types';

export type Operands = [Operand, Operand, Operand];
export class Instruction {
    public operands: Operands = [new Operand(), new Operand(), new Operand()];

    constructor(
        public operation: Operation = Operation.HLT,
        public width: Width = Width.Byte,
    ) {}

    public init(operation: Operation, width: Width) {
        this.operation = operation;
        this.width = width;
    }

    public set(
        index: Address,
        mode: Mode,
        value: Value,
        tag: Nullable<Tag> = null,
    ) {
        this.operands[index].mode = mode;
        this.operands[index].value = value;
        this.operands[index].tag = tag;
    }

    public clear() {
        this.operation = Operation.HLT;
        this.width = Width.Byte;
        this.set(0, Mode.Empty, 0);
        this.set(1, Mode.Empty, 0);
        this.set(2, Mode.Empty, 0);
    }
}

export class Operand {
    public mode = Mode.Empty;
    public value: Value = 0;
    public tag: Nullable<Tag> = null;
}
