import { Tag } from '../types';

export class Label {
    constructor(public readonly tag: Tag) {}
}
