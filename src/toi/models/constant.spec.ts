import { Width } from '../types';
import { Constant } from './constant';

describe('constant', () => {
    it('should have a tag, a width and a value', () => {
        expect(new Constant('a', Width.Byte, 1)).toMatchObject({
            tag: 'a',
            width: Width.Byte,
            value: 1,
        });
        expect(new Constant('a', Width.Word, 1)).toMatchObject({
            tag: 'a',
            width: Width.Word,
            value: 1,
        });
    });
});
