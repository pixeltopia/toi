import { Width } from '../types';
import { Constant } from './constant';
import { Inline } from './inline';
import { Instruction } from './instruction';
import { Label } from './label';
import { Statement } from './statement';

describe('statement', () => {
    it('should have a null label and a value', () => {
        const statement = new Statement();
        expect(statement.label).toBeNull();
        expect(statement.value).toBeNull();
    });

    it('should have a label', () => {
        expect(new Statement(new Label('a')).label?.tag).toEqual('a');
    });

    it('should have an instruction', () => {
        const instruction = new Instruction();
        const statement = new Statement(null, instruction);
        expect(statement.instruction).toBe(instruction);
        expect(statement.constant).toBeNull();
        expect(statement.inline).toBeNull();
    });

    it('should have a constant', () => {
        const constant = new Constant('a', Width.Byte, 1);
        const statement = new Statement(null, constant);
        expect(statement.instruction).toBeNull();
        expect(statement.constant).toBe(constant);
        expect(statement.inline).toBeNull();
    });

    it('should have an inline', () => {
        const inline = new Inline(Width.Byte, [1, 2, 3, 4]);
        const statement = new Statement(null, inline);
        expect(statement.instruction).toBeNull();
        expect(statement.constant).toBeNull();
        expect(statement.inline).toBe(inline);
    });
});
