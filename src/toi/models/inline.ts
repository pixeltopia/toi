import { Value, Width } from '../types';

export class InlineEncodingError extends Error {
    public constructor(
        public readonly index: number,
        public readonly value: string,
    ) {
        super(`encoding error at ${index} for character ${value}`);
    }
}

export class Inline {
    constructor(
        public readonly width: Width,
        public readonly values: Value[],
    ) {}

    public static makeBytes(value: string, terminate = true) {
        const values: Value[] = [];

        for (let index = 0; index < value.length; index++) {
            const charCode = value.charCodeAt(index);
            if (charCode > 0b01111111 || Number.isNaN(charCode)) {
                throw new InlineEncodingError(index, value.charAt(index));
            }
            values.push(charCode);
        }

        if (terminate) {
            values.push(0);
        }

        return new Inline(Width.Byte, values);
    }

    public static makeWords(value: string, terminate = true) {
        const values: Value[] = [];

        for (let index = 0; index < value.length; index++) {
            const charCode = value.charCodeAt(index);
            if (charCode > 0xffff || Number.isNaN(charCode)) {
                throw new InlineEncodingError(index, value.charAt(index));
            }
            values.push(charCode);
        }

        if (terminate) {
            values.push(0);
        }

        return new Inline(Width.Word, values);
    }
}
