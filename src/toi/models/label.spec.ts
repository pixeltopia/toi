import { Label } from './label';

describe('label', () => {
    it('should have a tag', () => {
        expect(new Label('a')).toEqual({
            tag: 'a',
        });
    });
});
