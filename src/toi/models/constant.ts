import { Nullable, Tag, Value, Width } from '../types';
import { Label } from './label';

export class Constant {
    constructor(
        public readonly tag: Tag,
        public readonly width: Width,
        public readonly value: Value,
        public readonly label: Nullable<Label> = null,
    ) {}
}
