export type Nullable<T> = T | null;
export type Tag = string;
export type Byte = number;
export type Word = number;
export type Value = Byte | Word;
export type Offset = number;
export type Address = number;
export type Memory = Uint8Array;
export type Reference = [Tag, Address, Width];
export type References = Reference[];

export const enum Operation {
    HLT = 0, // halt
    BRA, // branch always
    BEQ, // branch if equal
    BNE, // branch if not equal
    BLT, // branch if less than (signed)
    BGT, // branch if greater than (signed)
    BLE, // branch if less than or equal (signed)
    BGE, // branch if greater than or equal (signed)
    BLTU, // branch if less than (unsigned)
    BGTU, // branch if greater than (unsigned)
    BLEU, // branch if less than or equal (unsigned)
    BGEU, // branch if greater than or equal (unsigned)
    SLT, // set if less than (signed)
    SGT, // set if greater than (signed)
    SLTU, // set if less than (unsigned)
    SGTU, // set if greater than (unsigned)
    OR, // or
    AND, // and
    XOR, // exclusive or
    NOT, // not
    NEG, // negate
    ROL, // rotate left through carry
    ROR, // rotate right through carry
    LSL, // logical shift left
    LSR, // logical shift right
    ASR, // arithmetic (sign preserving) shift right
    EXT, // sign-preserving extend width
    ADD, // add
    SUB, // subtract
    MOV, // move
    PSH, // push
    POP, // pop
    JTS, // jump to subroutine
    RTS, // return from subroutine
    PSHM, // push many
    POPM, // pop many
    MOVM, // move many
}

export const enum Width {
    Byte = 0,
    Word = 1,
}

export const enum Mode {
    Empty = 0,
    Immediate = 1,
    Direct = 2,
    Indirect = 3,
}
