import { Memory } from '../types';
import { Stream } from './stream';

const makeMemory = (...values: number[]): Memory => new Uint8Array(values);

describe('stream', () => {
    it('should count the values', () => {
        const stream = new Stream(makeMemory(0, 1, 2, 3));
        expect(stream.count).toEqual(4);
    });

    it('should reset', () => {
        const stream = new Stream(makeMemory(0, 1, 2, 3));

        stream.jump(3);

        expect(stream.value).toEqual(3);

        stream.reset();

        expect(stream.memory).toEqual(makeMemory(0, 0, 0, 0));

        stream.value = 9;

        expect(stream.memory).toEqual(makeMemory(9, 0, 0, 0));
    });

    it('should set values', () => {
        const stream = new Stream(makeMemory(0, 1, 2, 3));

        stream.value = 3;
        expect(stream.memory).toEqual(makeMemory(3, 1, 2, 3));

        stream.skip(1);
        stream.value = 2;
        expect(stream.memory).toEqual(makeMemory(3, 2, 2, 3));

        stream.skip(1);
        stream.value = 1;
        expect(stream.memory).toEqual(makeMemory(3, 2, 1, 3));

        stream.skip(1);
        stream.value = 0;
        expect(stream.memory).toEqual(makeMemory(3, 2, 1, 0));
    });

    it('should skip over values', () => {
        const stream = new Stream(makeMemory(0, 1, 2, 3));
        expect(stream.value).toEqual(0);

        stream.skip(1);
        expect(stream.value).toEqual(1);

        stream.skip(1);
        expect(stream.value).toEqual(2);

        stream.skip(1);
        expect(stream.value).toEqual(3);

        stream.skip(-1);
        expect(stream.value).toEqual(2);
    });

    it('should jump to indexes', () => {
        const stream = new Stream(makeMemory(0, 1, 2, 3));

        stream.jump(3);
        expect(stream.value).toEqual(3);

        stream.jump(2);
        expect(stream.value).toEqual(2);

        stream.jump(1);
        expect(stream.value).toEqual(1);

        stream.jump(0);
        expect(stream.value).toEqual(0);
    });
});
