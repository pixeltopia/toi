import { Instruction } from '../models/instruction';
import { Byte, Mode, Width, Word } from '../types';
import { Stream } from './stream';

export class Reader extends Stream {
    public readWord(): Word {
        const hi = this.memory[this.address++];
        const lo = this.memory[this.address++];
        return (hi << 8) | lo;
    }

    public readByte(): Byte {
        return this.memory[this.address++];
    }

    public readInstruction(instruction: Instruction) {
        const encoded = this.readWord();
        const op = (encoded & 0b1111110000000000) >> 10;
        const wd = (encoded & 0b0000001000000000) >> 9;
        const m1 = (encoded & 0b0000000111000000) >> 6;
        const m2 = (encoded & 0b0000000000111000) >> 3;
        const m3 = (encoded & 0b0000000000000111) >> 0;
        instruction.operation = op;
        instruction.width = wd;
        instruction.operands[0].mode = m1;
        instruction.operands[1].mode = m2;
        instruction.operands[2].mode = m3;
        this.readOperands(instruction);
    }

    private readOperands({ width, operands }: Instruction) {
        for (const next of operands) {
            if (next.mode === Mode.Empty) {
                next.value = 0;
            } else if (next.mode === Mode.Immediate && width === Width.Byte) {
                next.value = this.readByte();
            } else {
                next.value = this.readWord();
            }
        }
    }
}
