import { Instruction, Operands } from '../models/instruction';
import {
    Address,
    Byte,
    Mode,
    Nullable,
    References,
    Tag,
    Width,
    Word,
} from '../types';
import { Stream } from './stream';

export class Writer extends Stream {
    public get numberOfBytesWritten() {
        return this.address;
    }

    public values(start: Address, end: Address) {
        return [...this.memory.slice(start, end)];
    }

    public get valuesWritten() {
        return [...this.memory.slice(0, this.address)];
    }

    public writeWords(values: Word[]) {
        values.forEach(this.writeWord.bind(this));
    }

    public writeWord(value: Word) {
        const hi = (value >> 8) & 0xff;
        const lo = (value >> 0) & 0xff;
        this.memory[this.address++] = hi;
        this.memory[this.address++] = lo;
    }

    public writeBytes(values: Byte[]) {
        values.forEach(this.writeByte.bind(this));
    }

    public writeByte(value: Byte) {
        this.memory[this.address++] = value & 0xff;
    }

    public writeInstruction(
        { operation, width, operands }: Instruction,
        references: References,
    ) {
        let encoded = 0;

        encoded += (operation & 0b111111) << 10;

        if (width === Width.Word) {
            encoded += 1 << 9;
        }

        const [{ mode: mode1 }, { mode: mode2 }, { mode: mode3 }] = operands;

        encoded += (mode1 & 0b111) << 6;
        encoded += (mode2 & 0b111) << 3;
        encoded += (mode3 & 0b111) << 0;

        this.writeWord(encoded);
        this.writeOperands(width, operands, references);
    }

    private writeOperands(
        width: Width,
        operands: Operands,
        references: References,
    ) {
        for (const next of operands) {
            if (next.mode === Mode.Empty) {
                continue;
            }

            const { tag } = next;

            if (next.mode === Mode.Immediate && width === Width.Byte) {
                this.add(tag, Width.Byte, references);
                this.writeByte(next.value);
            } else {
                this.add(tag, Width.Word, references);
                this.writeWord(next.value);
            }
        }
    }

    private add(tag: Nullable<Tag>, width: Width, references: References) {
        if (tag) {
            references.push([tag, this.numberOfBytesWritten, width]);
        }
    }
}
