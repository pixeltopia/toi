import { Instruction } from '../models/instruction';
import { makeInstruction } from '../test-helpers';
import { Mode, Operation, References, Width } from '../types';
import { Reader } from './reader';
import { Writer } from './writer';

let reader: Reader;
let instruction: Instruction;

const makeReader = (values: number[]): Reader =>
    new Reader(new Uint8Array(values));

describe('reader', () => {
    beforeEach(() => {
        instruction = new Instruction();
    });

    it('should read an instruction with no operands', () => {
        reader = makeReader(makeInstruction(Operation.HLT));
        reader.readInstruction(instruction);

        expect(instruction).toMatchObject({
            operation: Operation.HLT,
            width: Width.Byte,
            operands: [
                { mode: Mode.Empty, value: 0 },
                { mode: Mode.Empty, value: 0 },
                { mode: Mode.Empty, value: 0 },
            ],
        });
    });

    it('should read immediate bytes', () => {
        const i1 = [
            ...makeInstruction(Operation.ADD, Width.Byte, Mode.Immediate),
            0x12,
        ];
        const i2 = [
            ...makeInstruction(
                Operation.ADD,
                Width.Byte,
                Mode.Immediate,
                Mode.Immediate,
            ),
            0x12,
            0x34,
        ];

        const i3 = [
            ...makeInstruction(
                Operation.ADD,
                Width.Byte,
                Mode.Immediate,
                Mode.Immediate,
                Mode.Immediate,
            ),
            0x12,
            0x34,
            0x56,
        ];

        reader = makeReader([...i1, ...i2, ...i3]);

        reader.readInstruction(instruction);
        expect(instruction).toMatchObject({
            operation: Operation.ADD,
            width: Width.Byte,
            operands: [
                { mode: Mode.Immediate, value: 0x12 },
                { mode: Mode.Empty, value: 0 },
                { mode: Mode.Empty, value: 0 },
            ],
        });

        reader.readInstruction(instruction);
        expect(instruction).toMatchObject({
            operation: Operation.ADD,
            width: Width.Byte,
            operands: [
                { mode: Mode.Immediate, value: 0x12 },
                { mode: Mode.Immediate, value: 0x34 },
                { mode: Mode.Empty, value: 0 },
            ],
        });

        reader.readInstruction(instruction);
        expect(instruction).toMatchObject({
            operation: Operation.ADD,
            width: Width.Byte,
            operands: [
                { mode: Mode.Immediate, value: 0x12 },
                { mode: Mode.Immediate, value: 0x34 },
                { mode: Mode.Immediate, value: 0x56 },
            ],
        });
    });

    it('should read immediate words', () => {
        const i1 = [
            ...makeInstruction(Operation.ADD, Width.Word, Mode.Immediate),
            0x12,
            0x34,
        ];

        const i2 = [
            ...makeInstruction(
                Operation.ADD,
                Width.Word,
                Mode.Immediate,
                Mode.Immediate,
            ),
            0x12,
            0x34,
            0x56,
            0x78,
        ];

        const i3 = [
            ...makeInstruction(
                Operation.ADD,
                Width.Word,
                Mode.Immediate,
                Mode.Immediate,
                Mode.Immediate,
            ),
            0x12,
            0x34,
            0x56,
            0x78,
            0x9a,
            0xbc,
        ];

        reader = makeReader([...i1, ...i2, ...i3]);

        reader.readInstruction(instruction);
        expect(instruction).toMatchObject({
            operation: Operation.ADD,
            width: Width.Word,
            operands: [
                { mode: Mode.Immediate, value: 0x1234 },
                { mode: Mode.Empty, value: 0 },
                { mode: Mode.Empty, value: 0 },
            ],
        });

        reader.readInstruction(instruction);
        expect(instruction).toMatchObject({
            operation: Operation.ADD,
            width: Width.Word,
            operands: [
                { mode: Mode.Immediate, value: 0x1234 },
                { mode: Mode.Immediate, value: 0x5678 },
                { mode: Mode.Empty, value: 0 },
            ],
        });

        reader.readInstruction(instruction);
        expect(instruction).toMatchObject({
            operation: Operation.ADD,
            width: Width.Word,
            operands: [
                { mode: Mode.Immediate, value: 0x1234 },
                { mode: Mode.Immediate, value: 0x5678 },
                { mode: Mode.Immediate, value: 0x9abc },
            ],
        });
    });

    it.each([Mode.Direct, Mode.Indirect])(
        'should read addresses when mode is %s',
        (mode: Mode) => {
            const i1 = [
                ...makeInstruction(Operation.ADD, Width.Byte, mode),
                0x12,
                0x34,
            ];

            const i2 = [
                ...makeInstruction(Operation.ADD, Width.Byte, mode, mode),
                0x12,
                0x34,
                0x56,
                0x78,
            ];

            const i3 = [
                ...makeInstruction(Operation.ADD, Width.Byte, mode, mode, mode),
                0x12,
                0x34,
                0x56,
                0x78,
                0x9a,
                0xbc,
            ];

            reader = makeReader([...i1, ...i2, ...i3]);

            reader.readInstruction(instruction);
            expect(instruction).toMatchObject({
                operation: Operation.ADD,
                width: Width.Byte,
                operands: [
                    { mode, value: 0x1234 },
                    { mode: Mode.Empty, value: 0 },
                    { mode: Mode.Empty, value: 0 },
                ],
            });

            reader.readInstruction(instruction);
            expect(instruction).toMatchObject({
                operation: Operation.ADD,
                width: Width.Byte,
                operands: [
                    { mode, value: 0x1234 },
                    { mode, value: 0x5678 },
                    { mode: Mode.Empty, value: 0 },
                ],
            });

            reader.readInstruction(instruction);
            expect(instruction).toMatchObject({
                operation: Operation.ADD,
                width: Width.Byte,
                operands: [
                    { mode, value: 0x1234 },
                    { mode, value: 0x5678 },
                    { mode, value: 0x9abc },
                ],
            });
        },
    );

    it('should read immediate bytes and addresses', () => {
        reader = makeReader([
            ...makeInstruction(
                Operation.ADD,
                Width.Byte,
                Mode.Immediate,
                Mode.Direct,
                Mode.Indirect,
            ),
            0xab,
            0x12,
            0x34,
            0x56,
            0x78,
        ]);

        reader.readInstruction(instruction);
        expect(instruction).toMatchObject({
            operation: Operation.ADD,
            width: Width.Byte,
            operands: [
                { mode: Mode.Immediate, value: 0xab },
                { mode: Mode.Direct, value: 0x1234 },
                { mode: Mode.Indirect, value: 0x5678 },
            ],
        });
    });

    it('should read immediate words and addresses', () => {
        reader = makeReader([
            ...makeInstruction(
                Operation.ADD,
                Width.Word,
                Mode.Immediate,
                Mode.Direct,
                Mode.Indirect,
            ),
            0xab,
            0xcd,
            0x12,
            0x34,
            0x56,
            0x78,
        ]);

        reader.readInstruction(instruction);
        expect(instruction).toMatchObject({
            operation: Operation.ADD,
            width: Width.Word,
            operands: [
                { mode: Mode.Immediate, value: 0xabcd },
                { mode: Mode.Direct, value: 0x1234 },
                { mode: Mode.Indirect, value: 0x5678 },
            ],
        });
    });

    it('it should read what was written', () => {
        const i1 = new Instruction();
        const i2 = new Instruction();

        expect(i1).toEqual(i2);

        const memory = new Uint8Array(20);
        const writer = new Writer(memory);
        const reader = new Reader(memory);
        const references: References = [];

        i1.init(Operation.ADD, Width.Word);
        i1.set(0, Mode.Immediate, 0);
        i1.set(1, Mode.Direct, 1);
        i1.set(2, Mode.Indirect, 2);

        writer.writeInstruction(i1, references);
        expect(i1).not.toEqual(i2);

        reader.readInstruction(i2);
        expect(i1).toEqual(i2);
    });
});
