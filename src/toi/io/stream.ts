import { Address, Memory, Offset } from '../types';

export class Stream {
    constructor(
        public readonly memory: Memory,
        protected address: Address = 0,
    ) {}

    public get count() {
        return this.memory.length;
    }

    public get value() {
        return this.memory[this.address];
    }

    public set value(value: number) {
        this.memory[this.address] = value;
    }

    public reset() {
        this.memory.fill(0);
        this.address = 0;
    }

    public skip(offset: Offset) {
        this.address += offset;
    }

    public jump(address: Address) {
        this.address = address;
    }
}
