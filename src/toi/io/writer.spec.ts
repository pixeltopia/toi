import { Instruction } from '../models/instruction';
import { makeInstruction } from '../test-helpers';
import { Memory, Mode, Operation, References, Width } from '../types';
import { Writer } from './writer';

let memory: Memory;
let writer: Writer;
let instruction: Instruction;
let references: References;

let expected: number[];

const makeMemory = (): Memory => new Uint8Array(20);

describe('writer', () => {
    beforeEach(() => {
        memory = makeMemory();
        writer = new Writer(memory);
        instruction = new Instruction();
        references = [];
    });

    it('should not write void operands', () => {
        instruction.init(Operation.ADD, Width.Byte);
        instruction.set(0, Mode.Empty, 0);
        instruction.set(1, Mode.Empty, 0);
        instruction.set(2, Mode.Empty, 0);

        writer.writeInstruction(instruction, references);

        expected = makeInstruction(Operation.ADD, Width.Byte);

        expect(writer.valuesWritten).toEqual(expected);
    });

    it('should write immediate bytes', () => {
        instruction.init(Operation.ADD, Width.Byte);
        instruction.set(0, Mode.Immediate, 0x12);
        instruction.set(1, Mode.Empty, 0);
        instruction.set(2, Mode.Empty, 0);

        writer.writeInstruction(instruction, references);

        expected = [
            ...makeInstruction(Operation.ADD, Width.Byte, Mode.Immediate),
            0x12,
        ];

        expect(writer.valuesWritten).toEqual(expected);

        writer.reset();

        instruction.init(Operation.ADD, Width.Byte);
        instruction.set(0, Mode.Immediate, 0x12);
        instruction.set(1, Mode.Immediate, 0x34);
        instruction.set(2, Mode.Empty, 0);

        writer.writeInstruction(instruction, references);

        expected = [
            ...makeInstruction(
                Operation.ADD,
                Width.Byte,
                Mode.Immediate,
                Mode.Immediate,
            ),
            0x12,
            0x34,
        ];

        expect(writer.valuesWritten).toEqual(expected);

        writer.reset();

        instruction.init(Operation.ADD, Width.Byte);
        instruction.set(0, Mode.Immediate, 0x12);
        instruction.set(1, Mode.Immediate, 0x34);
        instruction.set(2, Mode.Immediate, 0x56);

        writer.writeInstruction(instruction, references);

        expected = [
            ...makeInstruction(
                Operation.ADD,
                Width.Byte,
                Mode.Immediate,
                Mode.Immediate,
                Mode.Immediate,
            ),
            0x12,
            0x34,
            0x56,
        ];

        expect(writer.valuesWritten).toEqual(expected);
    });

    it('should write immediate words', () => {
        instruction.init(Operation.ADD, Width.Word);
        instruction.set(0, Mode.Immediate, 0xabcd);
        instruction.set(1, Mode.Empty, 0);
        instruction.set(2, Mode.Empty, 0);

        writer.writeInstruction(instruction, references);

        expected = [
            ...makeInstruction(Operation.ADD, Width.Word, Mode.Immediate),
            0xab,
            0xcd,
        ];

        expect(writer.valuesWritten).toEqual(expected);

        writer.reset();

        instruction.init(Operation.ADD, Width.Word);
        instruction.set(0, Mode.Immediate, 0x1234);
        instruction.set(1, Mode.Immediate, 0x5678);
        instruction.set(2, Mode.Empty, 0);

        writer.writeInstruction(instruction, references);

        expected = [
            ...makeInstruction(
                Operation.ADD,
                Width.Word,
                Mode.Immediate,
                Mode.Immediate,
            ),
            0x12,
            0x34,
            0x56,
            0x78,
        ];

        expect(writer.valuesWritten).toEqual(expected);

        writer.reset();

        instruction.init(Operation.ADD, Width.Word);
        instruction.set(0, Mode.Immediate, 0x1234);
        instruction.set(1, Mode.Immediate, 0x5678);
        instruction.set(2, Mode.Immediate, 0x9abc);

        writer.writeInstruction(instruction, references);

        expected = [
            ...makeInstruction(
                Operation.ADD,
                Width.Word,
                Mode.Immediate,
                Mode.Immediate,
                Mode.Immediate,
            ),
            0x12,
            0x34,
            0x56,
            0x78,
            0x9a,
            0xbc,
        ];

        expect(writer.valuesWritten).toEqual(expected);
    });

    it('should write direct addresses', () => {
        instruction.init(Operation.ADD, Width.Byte);
        instruction.set(0, Mode.Direct, 0x1234);
        instruction.set(1, Mode.Empty, 0);
        instruction.set(2, Mode.Empty, 0);

        writer.writeInstruction(instruction, references);

        expected = [
            ...makeInstruction(Operation.ADD, Width.Byte, Mode.Direct),
            0x12,
            0x34,
        ];

        expect(writer.valuesWritten).toEqual(expected);

        writer.reset();

        instruction.init(Operation.ADD, Width.Byte);
        instruction.set(0, Mode.Direct, 0x1234);
        instruction.set(1, Mode.Direct, 0x5678);
        instruction.set(2, Mode.Empty, 0);

        writer.writeInstruction(instruction, references);

        expected = [
            ...makeInstruction(
                Operation.ADD,
                Width.Byte,
                Mode.Direct,
                Mode.Direct,
            ),
            0x12,
            0x34,
            0x56,
            0x78,
        ];

        expect(writer.valuesWritten).toEqual(expected);

        writer.reset();

        instruction.init(Operation.ADD, Width.Byte);
        instruction.set(0, Mode.Direct, 0x1234);
        instruction.set(1, Mode.Direct, 0x5678);
        instruction.set(2, Mode.Direct, 0x9abc);

        writer.writeInstruction(instruction, references);

        expected = [
            ...makeInstruction(
                Operation.ADD,
                Width.Byte,
                Mode.Direct,
                Mode.Direct,
                Mode.Direct,
            ),
            0x12,
            0x34,
            0x56,
            0x78,
            0x9a,
            0xbc,
        ];

        expect(writer.valuesWritten).toEqual(expected);
    });

    it('should write indirect addresses', () => {
        instruction.init(Operation.ADD, Width.Byte);
        instruction.set(0, Mode.Indirect, 0x1234);
        instruction.set(1, Mode.Empty, 0);
        instruction.set(2, Mode.Empty, 0);

        writer.writeInstruction(instruction, references);

        expected = [
            ...makeInstruction(Operation.ADD, Width.Byte, Mode.Indirect),
            0x12,
            0x34,
        ];

        expect(writer.valuesWritten).toEqual(expected);

        writer.reset();

        instruction.init(Operation.ADD, Width.Byte);
        instruction.set(1, Mode.Indirect, 0x5678);
        instruction.set(0, Mode.Indirect, 0x1234);
        instruction.set(2, Mode.Empty, 0);

        writer.writeInstruction(instruction, references);

        expected = [
            ...makeInstruction(
                Operation.ADD,
                Width.Byte,
                Mode.Indirect,
                Mode.Indirect,
            ),
            0x12,
            0x34,
            0x56,
            0x78,
        ];

        expect(writer.valuesWritten).toEqual(expected);

        writer.reset();

        instruction.init(Operation.ADD, Width.Byte);
        instruction.set(0, Mode.Indirect, 0x1234);
        instruction.set(1, Mode.Indirect, 0x5678);
        instruction.set(2, Mode.Indirect, 0x9abc);

        writer.writeInstruction(instruction, references);

        expected = [
            ...makeInstruction(
                Operation.ADD,
                Width.Byte,
                Mode.Indirect,
                Mode.Indirect,
                Mode.Indirect,
            ),
            0x12,
            0x34,
            0x56,
            0x78,
            0x9a,
            0xbc,
        ];

        expect(writer.valuesWritten).toEqual(expected);
    });

    it('should write immediate bytes and addresses', () => {
        instruction.init(Operation.ADD, Width.Byte);
        instruction.set(0, Mode.Immediate, 0xab);
        instruction.set(1, Mode.Direct, 0x1234);
        instruction.set(2, Mode.Indirect, 0x5678);

        writer.writeInstruction(instruction, references);

        expected = [
            ...makeInstruction(
                Operation.ADD,
                Width.Byte,
                Mode.Immediate,
                Mode.Direct,
                Mode.Indirect,
            ),
            0xab,
            0x12,
            0x34,
            0x56,
            0x78,
        ];

        expect(writer.valuesWritten).toEqual(expected);
    });

    it('should write immediate words and addresses', () => {
        instruction.init(Operation.ADD, Width.Word);
        instruction.set(0, Mode.Immediate, 0xabcd);
        instruction.set(1, Mode.Direct, 0x1234);
        instruction.set(2, Mode.Indirect, 0x5678);

        writer.writeInstruction(instruction, references);

        expected = [
            ...makeInstruction(
                Operation.ADD,
                Width.Word,
                Mode.Immediate,
                Mode.Direct,
                Mode.Indirect,
            ),
            0xab,
            0xcd,
            0x12,
            0x34,
            0x56,
            0x78,
        ];

        expect(writer.valuesWritten).toEqual(expected);
    });

    it('should write multiple instructions', () => {
        instruction.init(Operation.ADD, Width.Word);
        instruction.set(0, Mode.Immediate, 0xabcd);
        instruction.set(1, Mode.Direct, 0x1234);
        instruction.set(2, Mode.Indirect, 0x5678);

        writer.writeInstruction(instruction, references);

        instruction.init(Operation.SUB, Width.Byte);
        instruction.set(0, Mode.Immediate, 0xab);
        instruction.set(1, Mode.Direct, 0x1234);
        instruction.set(2, Mode.Indirect, 0x5678);

        writer.writeInstruction(instruction, references);

        instruction.clear();

        writer.writeInstruction(instruction, references);

        const i1 = [
            ...makeInstruction(
                Operation.ADD,
                Width.Word,
                Mode.Immediate,
                Mode.Direct,
                Mode.Indirect,
            ),
            0xab,
            0xcd,
            0x12,
            0x34,
            0x56,
            0x78,
        ];
        const i2 = [
            ...makeInstruction(
                Operation.SUB,
                Width.Byte,
                Mode.Immediate,
                Mode.Direct,
                Mode.Indirect,
            ),
            0xab,
            0x12,
            0x34,
            0x56,
            0x78,
        ];
        const i3 = makeInstruction(Operation.HLT, Width.Byte);
        expected = [...i1, ...i2, ...i3];

        expect(writer.valuesWritten).toEqual(expected);
    });
});
