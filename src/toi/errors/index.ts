import { Constant } from '../models/constant';
import { Instruction } from '../models/instruction';
import { Tag, Width } from '../types';

export class UnrepresentableValueError extends Error {
    constructor(value: string | number, expected: Width) {
        const kind = UnrepresentableValueError.makeKind(expected);
        super(`unable to represent ${value} as ${kind}`);
    }

    private static makeKind(expected: Width) {
        return expected === Width.Byte ? 'byte' : 'word';
    }
}

export class NaNError extends Error {
    constructor(value: string) {
        super(`${value} is NaN`);
    }
}

export class ReferenceError extends Error {
    constructor(tag: Tag) {
        super(`missing reference: ${tag}`);
    }
}

export class ReferenceWidthError extends Error {
    constructor(tag: Tag) {
        super(`constant is too wide to fit reference: ${tag}`);
    }
}

export class ExistingConstantError extends Error {
    constructor(existing: Constant, constant: Constant) {
        const { tag } = existing;
        const existingKind = ExistingConstantError.makeKind(existing);
        const constantKind = ExistingConstantError.makeKind(constant);
        super(`existing ${existingKind} for ${constantKind}: ${tag}`);
    }

    private static makeKind({ label }: Constant) {
        return label ? 'label' : 'constant';
    }
}

export class UnknownOperationError extends Error {
    constructor(operation: string) {
        super(`unknown operation: ${operation}`);
    }
}

export class InstructionFormatError extends Error {
    constructor(line: string, public instruction: Instruction) {
        super(`incorrect instruction format: ${line}`);
    }
}
