import {
    ExistingConstantError,
    NaNError,
    ReferenceError,
    ReferenceWidthError,
    UnknownOperationError,
    UnrepresentableValueError,
} from '.';
import { Constant } from '../models/constant';
import { Label } from '../models/label';
import { Tag, Width } from '../types';

const throws =
    (error: Error): (() => never) =>
    () => {
        throw error;
    };

const makeConstant = (tag: Tag) => new Constant(tag, Width.Byte, 1);
const makeLabelled = (tag: Tag) =>
    new Constant(tag, Width.Byte, 1, new Label(tag));

describe('errors', () => {
    it('should have unrepresentable number error messages', () => {
        expect(throws(new UnrepresentableValueError(1, Width.Byte))).toThrow(
            'unable to represent 1 as byte',
        );
        expect(throws(new UnrepresentableValueError(1, Width.Word))).toThrow(
            'unable to represent 1 as word',
        );
    });

    it('should have NaN error messages', () => {
        expect(throws(new NaNError('foo'))).toThrow('foo is NaN');
    });

    it('should have reference error messages', () => {
        expect(throws(new ReferenceError('a'))).toThrow('missing reference: a');
    });

    it('should have reference width error messages', () => {
        expect(throws(new ReferenceWidthError('a'))).toThrow(
            'constant is too wide to fit reference: a',
        );
    });

    it.each([
        [
            makeConstant('a'),
            makeConstant('a'),
            'existing constant for constant: a',
        ],
        [
            makeConstant('a'),
            makeLabelled('a'),
            'existing constant for label: a',
        ],
        [
            makeLabelled('a'),
            makeConstant('a'),
            'existing label for constant: a',
        ],
        [makeLabelled('a'), makeLabelled('a'), 'existing label for label: a'],
    ])(
        'should have existing constant error message',
        (existing: Constant, constant: Constant, message: string) => {
            expect(
                throws(new ExistingConstantError(existing, constant)),
            ).toThrow(message);
        },
    );

    it('should have unknown operation error messages', () => {
        expect(throws(new UnknownOperationError('xxx'))).toThrow(
            'unknown operation: xxx',
        );
    });
});
