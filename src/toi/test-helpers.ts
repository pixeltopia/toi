import { Mode, Operation, Width } from './types';

export const makeInstruction = (
    operation: Operation,
    width?: Width,
    ...modes: Mode[]
) => {
    let result = 0;

    result += (operation & 0b111111) << 10;

    if (width === Width.Word) {
        result += 1 << 9;
    }

    let offset = 6;
    for (const next of modes) {
        result += (next & 0b111) << offset;
        offset -= 3;
    }

    return makeWord(result);
};

export const makeWord = (value: number) => [
    (value >> 8) & 0xff,
    (value >> 0) & 0xff,
];
export const makeByte = (value: number) => [(value >> 0) & 0xff];
