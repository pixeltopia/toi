import { App } from './app/app';
import { Utils } from './app/utils';
import { View } from './app/view';

const utils = Utils.makeUtils(window);

window.onload = () => new App(utils, new View(utils)).start();
