import { Nullable } from '../toi/types';

export type Query = string | { selector: string; node?: HTMLElement };

export interface AnimationFrameCallback {
    performAnimationFrame: (timestamp: DOMHighResTimeStamp) => boolean;
}

export interface Props {
    kind: string;
    id?: string;
    className?: string;
}
export interface Resize {
    width?: string | number;
    height?: string | number;
}

export enum Format {
    Dec = 10,
    Hex = 16,
}

export class Utils {
    constructor(
        private readonly document: Document,
        private readonly crypto: Crypto,
        private readonly navigator: Navigator,
        private readonly storage: Storage,
    ) {}

    public static makeUtils({
        document,
        crypto,
        navigator,
        localStorage,
    }: Window = window) {
        return new Utils(document, crypto, navigator, localStorage);
    }

    public get makeIdentifier() {
        return this.crypto.getRandomValues(new Uint32Array(1))[0].toString(16);
    }

    public makeNode<T extends HTMLElement>(
        { kind, id = this.makeIdentifier, className }: Props,
        callback?: (node: T) => void,
    ): T {
        const result = this.document.createElement(kind) as T;
        result.id = id;
        if (className) {
            result.className = className;
        }
        if (callback) {
            callback(result);
        }
        return result;
    }

    public makeTextNode(value: string, callback?: (text: Text) => void) {
        const result = this.document.createTextNode(value);
        if (callback) {
            callback(result);
        }
        return result;
    }

    public findNode<T extends HTMLElement>(query: Query): T {
        let result: Nullable<T> = null;
        if (typeof query === 'string') {
            result = this.document.getElementById(query) as T;
        } else {
            const { selector, node } = query;
            result = (node || this.document).querySelector(selector) as T;
        }
        if (!result) {
            throw new Error('missing node');
        }
        return result;
    }

    public clearNode(node: HTMLElement) {
        while (node.firstChild) {
            node.removeChild(node.firstChild);
        }
    }

    public appendNode(node: HTMLElement, it: string | Node) {
        if (typeof it === 'string') {
            node.insertAdjacentHTML('beforeend', it.replace(/\s{2,}/, ''));
        } else {
            node.appendChild(it);
        }
    }

    public removeNode(node: HTMLElement, it: Node) {
        node.removeChild(it);
    }

    public size(node: HTMLElement, { width: w, height: h }: Resize) {
        if (w !== undefined) {
            node.style.width = typeof w === 'string' ? w : `${w}px`;
        }
        if (h !== undefined) {
            node.style.height = typeof h === 'string' ? h : `${h}px`;
        }
    }

    public format(
        value: number | null,
        format: Format,
        width: number,
        placeholder = '-',
    ) {
        return value === null
            ? placeholder.repeat(width)
            : value.toString(format).padStart(width, '0');
    }

    public isUserAgent(regex: RegExp) {
        return regex.test(this.navigator.userAgent);
    }

    public get canStyleScrollbars() {
        return !this.isUserAgent(/iPhone|iPad|iPod|Firefox/i);
    }

    public saveData(key: string, value: string) {
        if (this.storage) {
            try {
                this.storage.setItem(key, value);
                return true;
            } catch (error) {}
        }
        return false;
    }

    public loadData(key: string, defaultValue = '') {
        let stored = null;
        if (this.storage) {
            try {
                stored = this.storage.getItem(key);
            } catch (error) {}
        }
        return stored || defaultValue;
    }

    public isError<T>(value: T | Error): value is Error {
        return value instanceof Error;
    }

    public lineHeight(element: Element) {
        const value = this.document.defaultView?.getComputedStyle(
            element,
            null,
        ).lineHeight;
        if (!value) {
            return null;
        }
        const matched = value.match(/([0-9]+)/);
        if (!matched) {
            return null;
        }
        const [, height] = matched;
        return Number.parseInt(height);
    }

    public height(element: Element) {
        const value = this.document.defaultView?.getComputedStyle(
            element,
            null,
        ).height;
        if (!value) {
            return null;
        }
        const matched = value.match(/([0-9]+)/);
        if (!matched) {
            return null;
        }
        const [, height] = matched;
        return Number.parseInt(height);
    }

    public requestAnimationFrame(callback: AnimationFrameCallback) {
        const view = this.document.defaultView;
        if (!view) {
            return;
        }

        const fn = (timestamp: DOMHighResTimeStamp) => {
            if (callback.performAnimationFrame(timestamp)) {
                view.requestAnimationFrame(fn);
            }
        };

        view.requestAnimationFrame(fn);
    }
}
