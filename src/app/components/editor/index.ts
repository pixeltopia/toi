import { Nullable } from '../../../toi/types';
import { AnimationFrameCallback, Utils } from '../../utils';

export interface EditorObserver {
    didChangeText: (source: Editor) => void;
    didScroll: (source: Editor) => void;
    didChangeRow: (source: Editor) => void;
}

export interface ScrollOffset {
    x?: number;
    y?: number;
}

export enum EditorMode {
    ReadWrite,
    ReadOnly,
}

export class Editor implements AnimationFrameCallback {
    public currentRow: Nullable<number> = null;
    public currentRowOverride: Nullable<number> = null;

    private container: HTMLElement;
    private highlight: HTMLDivElement;
    private textarea: HTMLTextAreaElement;

    constructor(
        private readonly utils: Utils,
        private readonly observer: EditorObserver,
        id: string,
        placeholder: string,
        editorMode: EditorMode,
    ) {
        this.container = utils.findNode(id);
        this.container.className = 'editor';

        this.highlight = utils.makeNode({
            kind: 'div',
            className: 'highlight',
        });

        this.textarea = utils.makeNode({ kind: 'textarea' });
        this.textarea.placeholder = placeholder;
        this.textarea.wrap = 'off';
        this.textarea.autocomplete = 'off';
        this.textarea.autocapitalize = 'off';
        this.textarea.spellcheck = false;
        this.textarea.readOnly = editorMode === EditorMode.ReadOnly;
        this.textarea.oninput = () => observer.didChangeText(this);
        this.textarea.onscroll = () => observer.didScroll(this);

        this.textarea.style.position = 'relative';
        this.container.style.position = 'relative';

        this.textarea.style.zIndex = '1';
        this.highlight.style.zIndex = '0';

        utils.appendNode(this.container, this.textarea);
        utils.appendNode(this.container, this.highlight);
        utils.requestAnimationFrame(this);
    }

    public set value(value: string) {
        this.textarea.value = value;
        this.observer.didChangeText(this);
    }

    public get value() {
        return this.textarea.value;
    }

    public set scrollOffset({ x = 0, y = 0 }: ScrollOffset) {
        this.textarea.scrollLeft = x;
        this.textarea.scrollTop = y;
    }

    public get scrollOffset() {
        return { x: this.textarea.scrollLeft, y: this.textarea.scrollTop };
    }

    public set highlightRow(value: boolean) {
        if (value) {
            if (this.highlight.parentElement !== this.container) {
                this.utils.appendNode(this.container, this.highlight);
            }
        } else {
            this.utils.removeNode(this.container, this.highlight);
        }
    }

    public focus() {
        this.textarea.focus();
    }

    public performAnimationFrame(timestamp: DOMHighResTimeStamp) {
        const height = this.utils.lineHeight(this.textarea);
        if (height === null) {
            return false;
        }

        const row =
            this.currentRowOverride ??
            this.countNewlines(this.textarea.selectionStart);
        if (row === this.currentRow) {
            return true;
        }

        this.currentRow = row;
        this.observer.didChangeRow(this);

        const top = row * height - this.textarea.scrollTop;

        this.highlight.style.position = 'absolute';
        this.highlight.style.top = `${top}px`;
        this.highlight.style.left = '0';
        this.highlight.style.height = `${height}px`;
        this.highlight.style.width = `${this.textarea.clientWidth}px`;

        return true;
    }

    private countNewlines(upto: number) {
        const { value } = this;
        let result = 0;
        for (let index = 0; index < upto; index++) {
            if (value.charAt(index) === '\n') {
                result++;
            }
        }
        return result;
    }
}
