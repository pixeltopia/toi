import { Range } from '../../../toi/assembler/assembler';
import { Address, Memory } from '../../../toi/types';
import { Format, Utils } from '../../utils';

export class Slice {
    public baseAddress: Address = 0;

    constructor(
        public readonly memory: Memory,
        public readonly rows: number,
        public readonly cols: number,
    ) {}

    public addressAt(row: number, col = 0) {
        const address = this.baseAddress + (col + row * this.cols);
        return address < this.memory.length ? address : null;
    }

    public valueAt(row: number, col: number) {
        const address = this.addressAt(row, col);
        return address !== null && address < this.memory.length
            ? this.memory[address]
            : null;
    }
}

export enum Style {
    Code = 'code',
}

export type Span = [Style, Range];

export class Viewer {
    private container: HTMLElement;
    private scrolling: HTMLElement;
    private table: HTMLElement;

    private grid: HTMLElement[][] = [];
    private data: Text[][] = [];
    private addr: Text[] = [];

    private spans: Span[] = [];

    constructor(
        private readonly utils: Utils,
        private readonly slice: Slice,
        id: string,
    ) {
        this.container = this.utils.findNode(id);
        this.container.classList.add('viewer');
        this.scrolling = this.utils.makeNode({
            kind: 'div',
            className: 'scrolling',
        });
        this.table = this.utils.makeNode({ kind: 'table' });
        this.utils.appendNode(this.container, this.scrolling);
        this.utils.appendNode(this.scrolling, this.table);
        this.makeGrid();
        this.style();
        this.handleScrolling();
    }

    public addSpan(style: Style, start: Address, end: Address) {
        this.spans.push([style, [start, end]]);
    }

    public clearSpans() {
        this.spans = [];
    }

    public update() {
        for (let row = 0; row < this.slice.rows; row++) {
            const address = this.slice.addressAt(row);
            this.addr[row].nodeValue = this.utils.format(
                address,
                Format.Hex,
                4,
            );
            for (let col = 0; col < this.slice.cols; col++) {
                this.grid[row][col].className = 'data';
                const address = this.slice.addressAt(row, col);
                if (address !== null) {
                    this.stylesAt(address).forEach((style) => {
                        this.grid[row][col].classList.add(style);
                    });
                }
                this.data[row][col].nodeValue = this.utils.format(
                    this.slice.valueAt(row, col),
                    Format.Hex,
                    2,
                );
            }
        }
        return this;
    }

    private stylesAt(address: Address) {
        return this.spans
            .map(([style, [start, end]]) => {
                if (start <= address && address < end) {
                    return style;
                }
            })
            .filter((it): it is Style => !!it);
    }

    private makeGrid() {
        for (let row = 0; row < this.slice.rows; row++) {
            this.grid[row] = [];
            this.data[row] = [];
            this.utils.appendNode(
                this.table,
                this.utils.makeNode({ kind: 'tr' }, (tr) => {
                    this.utils.appendNode(
                        tr,
                        this.utils.makeNode(
                            { kind: 'td', className: 'addr' },
                            (td) => {
                                this.utils.makeTextNode(
                                    this.utils.format(0, Format.Hex, 4),
                                    (text) => {
                                        this.addr.push(text);
                                        this.utils.appendNode(td, text);
                                    },
                                );
                            },
                        ),
                    );
                    for (let col = 0; col < this.slice.cols; col++) {
                        this.utils.appendNode(
                            tr,
                            this.utils.makeNode(
                                { kind: 'td', className: 'data' },
                                (td) => {
                                    this.grid[row].push(td);
                                    this.utils.makeTextNode(
                                        this.utils.format(0, Format.Hex, 2),
                                        (text) => {
                                            this.data[row].push(text);
                                            this.utils.appendNode(td, text);
                                        },
                                    );
                                },
                            ),
                        );
                    }
                }),
            );
        }
    }

    private get containerHeight() {
        return this.utils.height(this.table) ?? 0;
    }

    private get scrollingHeight() {
        return this.containerHeight * 1000;
    }

    private style() {
        this.utils.size(this.container, { height: this.containerHeight });
        this.utils.size(this.scrolling, { height: this.scrollingHeight });
        if (this.utils.canStyleScrollbars) {
            this.utils.size(this.container, { width: 'max-content' });
        } else {
            this.utils.size(this.container, {
                width: this.table.clientWidth + 8,
            });
        }
    }

    private handleScrolling() {
        this.container.onscroll = ({ target }) => {
            if (!(target instanceof HTMLElement)) {
                return;
            }

            const { cols } = this.slice;

            const scrollClamped = Math.max(0, target.scrollTop);
            const scrollInset = this.scrollingHeight - this.containerHeight;
            const scalingRatio = scrollClamped / scrollInset;

            const addressInset = this.slice.memory.length;
            const scaledAddress = scalingRatio * addressInset;

            const rowOffset = Math.floor(scaledAddress / cols);
            const address = rowOffset * cols;

            this.slice.baseAddress = address;
            this.update();
        };
    }
}
