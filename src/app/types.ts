import { Entry } from '../toi/assembler/assembler';
import { Memory, Nullable } from '../toi/types';

export type Assembled = [number, Nullable<Entry>[]];

export interface UserInterface {
    start: (
        observer: UserInterfaceObserver,
        memory: Memory,
        source: string,
    ) => void;
    didAssemble: (value: Assembled | Error) => void;
}

export interface UserInterfaceObserver {
    didChangeEditor: (value: string) => void;
}
