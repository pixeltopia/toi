import { Memory } from '../../toi/types';
import { Editor, EditorMode, EditorObserver } from '../components/editor';
import { Slice, Style, Viewer } from '../components/viewer';
import { Assembled, UserInterface, UserInterfaceObserver } from '../types';
import { Utils } from '../utils';

enum Identifiers {
    Editor = 'assembler-editor',
    Output = 'assembler-output',
    Viewer = 'assembler-viewer',
}

export class View implements UserInterface, EditorObserver {
    private observer?: UserInterfaceObserver;

    private viewer?: Viewer;
    private editor?: Editor;
    private output?: Editor;

    constructor(private readonly utils: Utils) {}

    public start(
        observer: UserInterfaceObserver,
        memory: Memory,
        source: string,
    ) {
        this.observer = observer;

        this.viewer = new Viewer(
            this.utils,
            new Slice(memory, 14, 16),
            Identifiers.Viewer,
        ).update();
        this.editor = new Editor(
            this.utils,
            this,
            Identifiers.Editor,
            'assembler editor',
            EditorMode.ReadWrite,
        );
        this.output = new Editor(
            this.utils,
            this,
            Identifiers.Output,
            'assembler output',
            EditorMode.ReadOnly,
        );

        this.editor.value = source;
        this.editor.focus();
    }

    public didChangeText(source: Editor) {
        if (source === this.editor && this.output && this.observer) {
            this.observer.didChangeEditor(source.value);
        }
    }

    public didChangeRow(source: Editor) {
        if (source === this.editor && this.output) {
            this.output.currentRowOverride = source.currentRow;
        }
    }

    public didScroll(source: Editor) {
        if (source === this.editor && this.output) {
            this.output.scrollOffset = { y: source.scrollOffset.y };
        }
    }

    public didAssemble(value: Assembled | Error) {
        if (!this.output) {
            return;
        }

        if (this.utils.isError(value)) {
            this.output.value = `${value}`;
            this.output.highlightRow = false;
            return;
        }

        this.output.highlightRow = true;

        const [numberOfBytesWritten, entries] = value;

        this.viewer?.clearSpans();
        this.viewer?.addSpan(Style.Code, 0, numberOfBytesWritten);
        this.viewer?.update();

        if (entries.length === 1 && entries[0] === null) {
            this.output.value = '';
            return;
        }

        this.output.value = entries
            .map((entry) => {
                if (entry) {
                    const [start, data] = entry;
                    return `${start}:${data}`;
                } else {
                    return ' '.repeat(4) + ':';
                }
            })
            .join('\n');
    }
}
