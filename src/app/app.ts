import { Assembler } from '../toi';
import { Entry } from '../toi/assembler/assembler';
import { Nullable } from '../toi/types';
import { UserInterface, UserInterfaceObserver } from './types';
import { Utils } from './utils';

enum Keys {
    Source = 'source',
}

export class App implements UserInterfaceObserver {
    private memory = new Uint8Array(Math.pow(2, 16));
    private assember = new Assembler(this.memory);

    constructor(
        private readonly utils: Utils,
        private readonly view: UserInterface,
    ) {}

    public start() {
        this.view.start(this, this.memory, this.utils.loadData(Keys.Source));
    }

    public didChangeEditor(value: string) {
        this.utils.saveData(Keys.Source, value);
        try {
            const entries: Nullable<Entry>[] = [];
            this.assember.reset();
            this.assember.assemble(value, (entry) => {
                entries.push(entry);
            });
            this.view.didAssemble([
                this.assember.numberOfBytesWritten,
                entries,
            ]);
        } catch (error) {
            if (this.utils.isError(error)) {
                this.view.didAssemble(error);
            }
        }
    }
}
