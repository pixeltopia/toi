module.exports = {
    preset: "ts-jest",
    testEnvironment: "node",
    testPathIgnorePatterns: ["<rootDir>/node_modules/", "<rootDir>/dist"],
    collectCoverageFrom: ["<rootDir>/src/**/*.ts"],
    roots: ["<rootDir>"],
    modulePaths: ["<rootDir>"],
    moduleDirectories: ["node_modules"],
    // Parcel ~ alias for testing
    moduleNameMapper: {
        "~(.*)$": "<rootDir>/src/$1",
    }
};